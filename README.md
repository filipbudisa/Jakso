# Jakso

Portal za praćenje studentskih sekcija. Studentski projekt iz predmeta Programske paradigme i jezici.

# Tekst zadatka

Aplikaciji se može pristupiti isključivo s korisničkim računom. Registrirati se može bilo tko. Kod registracije je potrebno unijeti ime, prezime, datum rođenja, email, lozinku i fakultet.

Prijavljeni korisnici mogu pregledavati profile ostalih korisnika, javne stranice svih sekcija, te unutarnje stranice sekcija u kojima su članovi.

Korisnici mogu poslati prijavu za članstvo u bilo koju sekciju koja trenutno prima članove pri čemu unose tekst prijave.

Sekcije imaju naziv, opis, obavijesti, termine i lokacije (tekstualno polje), te događaje.

Terminima se mogu dodati izmjene koje se ispisuju podebljano ispod ispisa termina te služe za izvanredne promjene određenih termina. Kod unosa obavijesti termina se odabire do kad će se ta obavijest prikazivati tako da se obavijest više ne prikazuje nakog tog termina.

Uz događaje se upisuje lokacije i vrijeme, a članovi mogu označiti mogu li doći ili ne.

Članovi su još i opcionalno razvrstani u grupe, te se obavijesti, događaji, i termini mogu opcionalno postaviti po grupi.

U sekciji su tri razine hijerarhije. Voditelji, koordinatori i članovi. Članovi mogu pregledavati obavijesti, termine, događaje i članove sekcije te svoje grupe u sekciji. Koordinatori uz to vide i mogu uređivati te dodavati sve obavijesti, termine, događaje, članove i grupe, uz što mogu i premještati članove iz grupe u grupu. Mogu mijenjati i opis sekcije i termine, kao i obavijesti termina te pregledavati i odobriti ili odbiti prijave u sekciju. Voditelji uz to sve mogu i obrisati sekciju i podnijeti zahtjev za mijenjanje imena sekcije.

Na aktivnoj prijavi vođe i koordinatori mogu komunicirati s korisnikom koji ju je podnio dok se na prijavu ne odgovori (primljen/odbijen).

Sekcije imaju pripadni fakultet, te članovi mogu pretraživati sekcije po fakultetima.

Na vanjskoj stranici sekcije, koja se prikazuje korisnicima koji nisu članovi sekcije, se prikazuje samo naziv sekcije, opis, pripadni fakultet, te prima li trenutno nove članove. Ukoliko sekcija prima nove članove, prikazuje se i opcija za prijavu za članstvo u sekciju.

Svaki član može osnovati sekciju, pri čemu je potrebno unijeti samo naziv i pripadni fakultet, dok se ostali podaci naknadno mogu uređivati. Član koji osnuje sekciju je automatski voditelj sekcije.

Koordinatori i vođe mogu međusobno slati poruke. Koordinatori i vođe također mogu razmjenjivati poruke s ostalim članovima sekcije.