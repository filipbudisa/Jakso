import express from 'express';
import passport from 'passport';
import bodyParser from 'body-parser';
import session from 'express-session';
import cors from 'cors';

import login from './routes/login';
import user from './routes/user';
import section from './routes/section';
import application from './routes/application';

var app = express();
app.use(express.static('public'));
app.set('views', __dirname + '/../views');
app.set('view engine', 'pug');
app.use(cors({ credentials: true, origin: ["http://jakso.sprint.ninja", /localhost/] }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(
	session({
		secret: 'secret',
		saveUninitialized: true,
		resave: true
	})
);
app.use(passport.initialize());
app.use(passport.session());

let auth = (req, res, next) => {
	if (req.isAuthenticated()) return next();
	res.json({ code: -2 });
};

app.use(login);
app.use(auth, user);
app.use(auth, section);
app.use(auth, application);

app.listen(8080, console.log('\n\nServer is ready...'));
