import mongoose, { mongo } from 'mongoose';
import { User, Section, Application } from './models';
import { mongodb } from '../properties';
import { getUserById } from './userDB';
import { getApplicationsByUserId } from './applicationDB';

export const getSectionById = (id, callback) => {
	mongoose.connect(mongodb, () => {
		Section.find({ _id: id }, (err, sections) => {
			let section = sections.length == 0 ? null : sections[0];
			if (!section) return callback(null);

			if (!section.calendar) section.calendar = {};
			if (!section.calendar.regularWeekly) section.calendar.regularWeekly = [];
			if (!section.calendar.regularWeeklyChange) section.calendar.regularWeeklyChange = [];
			if (!section.calendar.events) section.calendar.events = [];
			if (!section.acceptingMembers) section.acceptingMembers = false;

			section.calendar.regularWeekly = section.calendar.regularWeekly.sort((a, b) => {
				let dowDiff = a.dow - b.dow;
				if (dowDiff == 0) return Date.parse(a.time) - Date.parse(b.time);
				return dowDiff;
			});

			section.calendar.regularWeeklyChange = section.calendar.regularWeeklyChange.sort((a, b) => {
				let dowDiff = a.dow - b.dow;
				if (dowDiff == 0) return Date.parse(a.time) - Date.parse(b.time);
				return dowDiff;
			});

			section.calendar.events = section.calendar.events.sort((a, b) => Date.parse(a.time) - Date.parse(b.time));

			section.uid = section._id;
			callback(section);
		});
	});
};

export const getSectionByIdIncludeUser = (id, callback) => {
	mongoose.connect(mongodb, () => {
		Section.find({ _id: id }, (err, sections) => {
			let section = sections.length == 0 ? null : sections[0];
			if (!section) return callback(null);
			section.uid = section._id;

			if (!section.acceptingMembers) section.acceptingMembers = false;

			if (!section.members) section.members = [];
			User.find({ _id: { $in: section.members } }, (err, users) => {
				users = users.map(u => {
					u.uid = u._id;
					return u;
				});
				section.members = section.members.map(member => users.filter(u => u._id.toString() == member.toString())[0]);
				callback(section);
			});
		});
	});
};

export const getSectionByIdIncludeUserAndApplicationCheck = (id, userId, callback) => {
	mongoose.connect(mongodb, () => {
		Section.find({ _id: id }, (err, sections) => {
			let section = sections.length == 0 ? null : sections[0];
			if (!section) return callback(null);
			section.uid = section._id;

			if (!section.acceptingMembers) section.acceptingMembers = false;

			if (!section.members) section.members = [];
			User.find({ _id: { $in: section.members } }, (err, users) => {
				users = users.map(u => {
					u.uid = u._id;
					return u;
				});
				section.members = section.members.map(member => users.filter(u => u._id.toString() == member.toString())[0]);

				if (section.members.map(m => m._id.toString()).indexOf(userId.toString()) == -1) {
					getApplicationsByUserId(userId, applications => {
						applications = applications.filter(a => a.sectionUid == id);
						if (applications.length == 0) return callback(section, false, false, null);
						callback(section, false, true, applications[0]);
					});
				} else callback(section, true, false, null);
			});
		});
	});
};

export const newSection = (name, description, location, accepting, creator, callback) => {
	mongoose.connect(mongodb, () => {
		let section = new Section({ name, description, location, accepting, creator });

		section.save((err, product) => {
			if (err) return callback(null);

			User.find({ _id: creator }, (err, users) => {
				let user = users.length == 0 ? null : users[0];
				if (!user) return callback(null);
				if (!user.joinedSections) user.joinedSections = [];
				user.joinedSections.push(product._id);

				User.update({ _id: user._id }, { joinedSections: user.joinedSections }, (err, n, raw) => {
					if (err) return callback(null);
					return callback(product._id);
				});
			});
		});
	});
};

export const deleteSection = (uid, callback) => {
	mongoose.connect(mongodb, () => {
		User.find({ joinedSections: uid }, (err, users) => {
			for (let user of users) {
				user.joinedSections = user.joinedSections.filter(s => s.toString() != uid.toString());
				User.updateOne({ _id: user._id }, { joinedSections: user.joinedSections }, (err, n, raw) => {
					if (err) return callback(null);
					Section.remove({ _id: uid }, err => {
						if (err) return callback(null);
						Application.remove({ sectionUid: uid }, err => {
							callback(true);
						});
					});
				});
			}
		});
	});
};

export const memberAction = (userId, sectionId, action, callback) => {
	mongoose.connect(mongodb, () => {
		Section.find({ _id: sectionId }, (err, sections) => {
			if (sections.length == 0) return callback(null);

			let section = sections[0];
			if (!section.members) section.members = [];

			let i = section.members.map(m => m.toString()).indexOf(userId.toString());

			if (action.toLowerCase() == 'add') {
				if (i != -1) return callback(true);
				section.members.push(userId);
			} else {
				if (i == -1) return callback(true);
				section.members.splice(i, 1);
			}

			Section.update({ _id: sectionId }, { members: section.members }, (err, n, raw) => {
				if (err) return callback(null);
				User.find({ _id: userId }, (err, users) => {
					let user = users.length == 0 ? null : users[0];
					if (!user) return callback(null);
					if (!user.joinedSections) user.joinedSections = [];

					if (action.toLowerCase() == 'add') user.joinedSections.push(sectionId);
					else user.joinedSections = user.joinedSections.filter(s => s.toString() != sectionId.toString());

					User.update({ _id: user._id }, { joinedSections: user.joinedSections }, (err, n, raw) => {
						if (err) return callback(null);
						return callback(true);
					});
				});
			});
		});
	});
};

export const weeklyAction = (dayOfTheWeek, time, weeklyId, sectionId, action, callback) => {
	mongoose.connect(mongodb, () => {
		Section.find({ _id: sectionId }, (err, sections) => {
			if (sections.length == 0) return callback(null);

			let section = sections[0];
			if (!section.calendar) section.calendar = {};
			if (!section.calendar.regularWeekly) section.calendar.regularWeekly = [];

			let weeklyUid = mongoose.Types.ObjectId();
			if (action.toLowerCase() == 'add') section.calendar.regularWeekly.push({ uid: weeklyUid, dow: dayOfTheWeek, time });
			else section.calendar.regularWeekly.splice(section.calendar.regularWeekly.map(w => w.uid.toString()).indexOf(weeklyId.toString()), 1);

			Section.update({ _id: sectionId }, { calendar: section.calendar }, (err, n, raw) => {
				if (err) return callback(null);
				return callback(weeklyUid);
			});
		});
	});
};

export const weeklyChangeAction = (dow, time, canceled, weeklyId, sectionId, action, callback) => {
	mongoose.connect(mongodb, () => {
		Section.find({ _id: sectionId }, (err, sections) => {
			if (sections.length == 0) return callback(null);

			let section = sections[0];
			if (!section.calendar) section.calendar = {};
			if (!section.calendar.regularWeeklyChange) section.calendar.regularWeeklyChange = [];

			if (action.toLowerCase() == 'add') section.calendar.regularWeeklyChange.push({ uid: weeklyId, dow, time, canceled });
			else section.calendar.regularWeeklyChange.splice(section.calendar.regularWeeklyChange.map(w => w.uid.toString()).indexOf(weeklyId.toString()), 1);

			Section.update({ _id: sectionId }, { calendar: section.calendar }, (err, n, raw) => {
				if (err) return callback(null);
				return callback(true);
			});
		});
	});
};

export const addEvent = (name, time, location, text, required, sectionId, callback) => {
	mongoose.connect(mongodb, () => {
		Section.find({ _id: sectionId }, (err, sections) => {
			if (sections.length == 0) return callback(null);

			let section = sections[0];
			if (!section.calendar) section.calendar = {};
			if (!section.calendar.events) section.calendar.events = [];

			let eventUid = mongoose.Types.ObjectId();
			section.calendar.events.push({ uid: eventUid, name, time, location, text, required });

			Section.update({ _id: sectionId }, { calendar: section.calendar }, (err, n, raw) => {
				if (err) return callback(null);
				return callback(eventUid);
			});
		});
	});
};

export const removeEvent = (eventId, sectionId, callback) => {
	mongoose.connect(mongodb, () => {
		Section.find({ _id: sectionId }, (err, sections) => {
			if (sections.length == 0) return callback(null);

			let section = sections[0];
			if (!section.calendar) section.calendar = {};
			if (!section.calendar.events) section.calendar.events = [];

			let i = section.calendar.events.map(e => e.uid.toString()).indexOf(eventId.toString());
			if (i == -1) return callback(true);

			section.calendar.events.splice(i, 1);

			Section.update({ _id: sectionId }, { calendar: section.calendar }, (err, n, raw) => {
				if (err) return callback(null);
				return callback(true);
			});
		});
	});
};

export const confirmEvent = (userId, sectionId, eventId, coming, callback) => {
	mongoose.connect(mongodb, () => {
		Section.find({ _id: sectionId }, (err, sections) => {
			if (sections.length == 0) return callback(null);

			let section = sections[0];
			if (!section.calendar) return callback(null);
			if (!section.calendar.events) return callback(null);
			section.uid = section._id;

			let i = section.calendar.events.map(e => (e.uid || e._id).toString()).indexOf(eventId.toString());
			if (i == -1) return callback(null);

			if (!section.calendar.events[i].members) section.calendar.events[i].members = [];
			if (coming) section.calendar.events[i].members.push(userId);
			else section.calendar.events[i].members = section.calendar.events[i].members.filter(e => e.toString() != userId.toString());

			Section.update({ _id: sectionId }, { calendar: section.calendar }, (err, n, raw) => {
				if (err) return callback(null);
				return callback(true);
			});
		});
	});
};

export const notificationAction = (title, text, sectionId, notificationId, action, callback) => {
	mongoose.connect(mongodb, () => {
		Section.find({ _id: sectionId }, (err, sections) => {
			if (sections.length == 0) return callback(null);

			let section = sections[0];
			if (!section.notifications) section.notifications = [];

			let notificationUid = mongoose.Types.ObjectId();
			if (action.toLowerCase() == 'add') section.notifications.push({ uid: notificationUid, title, text, time: new Date() });
			else section.notifications.splice(section.notifications.map(n => n.uid.toString()).indexOf(notificationId.toString()), 1);

			Section.update({ _id: sectionId }, { notifications: section.notifications }, (err, n, raw) => {
				if (err) return callback(null);
				return callback(notificationUid);
			});
		});
	});
};

export const searchSections = (query, callback) => {
	mongoose.connect(mongodb, () => {
		Section.find({ $or: [{ name: { $regex: query, $options: 'i' } }, { location: { $regex: query, $options: 'i' } }] }, (err, sections) => {
			sections = sections.map(section => ({ uid: section._id, name: section.name, location: section.location }));
			callback(sections);
		});
	});
};

export const editSection = (id, name, description, location, accepting, callback) => {
	mongoose.connect(mongodb, () => {
		Section.find({ _id: id }, (err, sections) => {
			if (sections.length == 0) return callback(null);

			let changes = {};
			if (name) Object.assign(changes, { name });
			if (description) Object.assign(changes, { description });
			if (location) Object.assign(changes, { location });
			if (accepting) Object.assign(changes, { acceptingMembers: accepting });

			Section.update({ _id: id }, changes, (err, n, raw) => {
				if (err) return callback(null);
				return callback(true);
			});
		});
	});
};
