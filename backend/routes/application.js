import express from 'express';

import * as ApplicationDB from './applicationDB';
import * as SectionDB from './sectionDB';

const router = express.Router();
export default router;

router.post('/new-application', (req, res) => {
	let { sectionUid, userUid, text } = req.body;
	ApplicationDB.newApplication(userUid, sectionUid, text, applicationUid => res.json(!applicationUid ? { code: -1 } : { code: 0, applicationUid }));
});

router.post('/get-applications', (req, res) => {
	let { sectionUid, userUid } = req.body;
	if (sectionUid) ApplicationDB.getApplicationsBySectionId(sectionUid, applications => res.json({ code: !applications ? -1 : 0, applications }));
	else if (userUid) ApplicationDB.getApplicationsByUserId(userUid, applications => res.json({ code: !applications ? -1 : 0, applications }));
});

router.post('/get-application', (req, res) => {
	let { applicationUid } = req.body;
	ApplicationDB.getApplicationById(applicationUid, application => res.json({ code: !application ? -1 : 0, application }));
});

router.post('/application-message', (req, res) => {
	let { userUid, applicationUid, text } = req.body;
	ApplicationDB.applicationMessage(userUid, applicationUid, text, status => res.json({ code: status ? 0 : 1 }));
});

router.post('/application-respond', (req, res) => {
	let { accept, applicationUid } = req.body;
	ApplicationDB.applicationRespond(applicationUid, accept, status => {
		if (!status) return res.json({ code: -1 });

		if (accept) {
			ApplicationDB.getApplicationById(applicationUid, application => {
				if (!application) return res.json({ code: -1 });
				SectionDB.memberAction(application.userUid, application.sectionUid, 'Add', status => res.json({ code: status ? 0 : -1 }));
			});
		}
		else res.json({ code: 0 });
	});
});
