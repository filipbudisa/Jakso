import mongoose, { Schema } from 'mongoose';

const arrayOfOids = { type: [Schema.Types.ObjectId], default: [] };

const userSchema = new Schema({
	uid: Schema.Types.ObjectId,
	name: String,
	surname: String,
	email: String,
	password: String,
	birthDate: Date,
	joinedSections: arrayOfOids
});
export const User = mongoose.model('User', userSchema);

const sectionSchema = new Schema({
	uid: Schema.Types.ObjectId,
	name: String,
	description: String,
	creator: Schema.Types.ObjectId,
	location: String,
	calendar: Schema.Types.Mixed,
	members: arrayOfOids,
	notifications: Schema.Types.Mixed,
	acceptingMembers: Boolean
});
export const Section = mongoose.model('Section', sectionSchema);

const applicationSchema = new Schema({
	uid: Schema.Types.ObjectId,
	text: String,
	userUid: Schema.Types.ObjectId,
	userName: String,
	sectionName: String,
	sectionCreatorUid: Schema.Types.ObjectId,
	sectionUid: Schema.Types.ObjectId,
	status: Number,
	messages: Schema.Types.Mixed
});
export const Application = mongoose.model('Application', applicationSchema);
