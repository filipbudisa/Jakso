import mongoose, { mongo } from 'mongoose';
import { Application } from './models';
import { mongodb } from '../properties';
import { getUserById } from './userDB';
import { getSectionById } from './sectionDB';

export const getApplicationById = (id, callback) => {
	mongoose.connect(mongodb, () => {
		Application.find({ uid: id }, (err, applications) => {
			let application = applications.length == 0 ? null : applications[0];
			if (!application) return callback(null);
			application.messages = application.messages.sort((a, b) => Date.parse(b.time) - Date.parse(a.time));
			callback(application);
		});
	});
};

export const getApplicationsByUserId = (userUid, callback) => {
	mongoose.connect(mongodb, () => {
		Application.find({ userUid }, (err, applications) => {
			if (applications.length == 0) return callback([]);
			applications = applications.map(application => {
				application.messages = application.messages.sort((a, b) => Date.parse(b.time) - Date.parse(a.time));
				return application;
			});
			callback(applications);
		});
	});
};

export const getApplicationsBySectionId = (sectionUid, callback) => {
	mongoose.connect(mongodb, () => {
		Application.find({ sectionUid }, (err, applications) => {
			if (applications.length == 0) return callback([]);
			applications = applications.map(application => {
				application.messages = application.messages.sort((a, b) => Date.parse(b.time) - Date.parse(a.time));
				return application;
			});
			callback(applications);
		});
	});
};

export const newApplication = (userUid, sectionUid, text, callback) => {
	mongoose.connect(mongodb, () => {
		getSectionById(sectionUid, section => {
			if (!section) return callback(null);

			getUserById(userUid, user => {
				if (!user) return callback(null);
				let application = new Application({
					text,
					userUid,
					sectionUid,
					userName: user.name + ' ' + user.surname,
					sectionName: section.name,
					sectionCreatorUid: section.creator,
					status: 0,
					messages: []
				});

				application.save((err, product) => {
					if (err) return callback(null);
					Application.updateOne({ _id: product._id }, { uid: product._id }, (err, raw) => {
						if (err) return callback(null);
						callback(product._id);
					});
				});
			});
		});
	});
};

export const applicationMessage = (userId, applicationId, text, callback) => {
	mongoose.connect(mongodb, () => {
		Application.find({ _id: applicationId }, (err, applications) => {
			let application = applications.length == 0 ? null : applications[0];
			if (!application) return callback(null);

			if (!application.messages) application.messages = [];
			application.messages.push({ sender: userId, text, time: new Date() });

			Application.updateOne({ _id: applicationId }, { messages: application.messages }, (err, raw) => {
				if (err) return callback(null);
				return callback(true);
			});
		});
	});
};

export const applicationRespond = (applicationId, accept, callback) => {
	mongoose.connect(mongodb, () => {
		Application.find({ _id: applicationId }, (err, applications) => {
			let application = applications.length == 0 ? null : applications[0];
			if (!application) return callback(null);

			application.status = accept ? 2 : 1;

			Application.updateOne({ _id: applicationId }, { status: application.status }, (err, raw) => {
				if (err) return callback(null);
				return callback(true);
			});
		});
	});
};
