import express from 'express';

import * as SectionDB from './sectionDB';
import { pageSize } from '../properties';

const router = express.Router();
export default router;

const checkSectionIdCreator = (req, res, next) => {
	SectionDB.getSectionById(req.body.sectionUid, section => {
		if (section.creator.toString() != req.user._id.toString()) return res.json({ code: -3 });
		next();
	});
};

router.post('/get-section', (req, res) => {
	let { uid } = req.body;

	SectionDB.getSectionByIdIncludeUserAndApplicationCheck(uid, req.user._id, (section, isMember, hasApplication, application) => {
		if (!section) return res.json({ code: 1 });
		res.json({ code: 0, section, isMember, hasApplication, application });
	});
});

router.post('/new-section', (req, res) => {
	let { name, description, location, accepting, creator } = req.body;
	SectionDB.newSection(name, description, location, accepting, creator, sectionUid => res.json(!sectionUid ? { code: -1 } : { code: 0, sectionUid }));
});

router.post('/delete-section', (req, res) => {
	let { uid } = req.body;
	SectionDB.getSectionById(req.body.uid, section => {
		if (section.creator.toString() != req.user._id.toString()) return res.json({ code: -3 });
		SectionDB.deleteSection(uid, status => res.json({ code: status ? 0 : -1 }));
	});
});

router.post('/new-member', checkSectionIdCreator, (req, res) => {
	let { userUid, sectionUid } = req.body;
	SectionDB.memberAction(userUid, sectionUid, 'Add', status => res.json({ code: status ? 0 : -1 }));
});

router.post('/remove-member', (req, res) => {
	let { userUid, sectionUid } = req.body;
	if (req.user._id.toString() != userUid.toString())
		return checkSectionIdCreator(req, res, () => SectionDB.memberAction(userUid, sectionUid, 'Delete', status => res.json({ code: status ? 0 : -1 })));
	SectionDB.memberAction(userUid, sectionUid, 'Delete', status => res.json({ code: status ? 0 : -1 }));
});

router.post('/new-weekly', checkSectionIdCreator, (req, res) => {
	let { dayOfTheWeek, time, sectionUid } = req.body;
	SectionDB.weeklyAction(dayOfTheWeek, time, null, sectionUid, 'Add', weeklyUid => res.json(!weeklyUid ? { code: -1 } : { code: 0, weeklyUid }));
});

router.post('/remove-weekly', checkSectionIdCreator, (req, res) => {
	let { weeklyUid, sectionUid } = req.body;
	SectionDB.weeklyAction(null, null, weeklyUid, sectionUid, 'Delete', status => res.json({ code: status ? 0 : -1 }));
});

router.post('/new-weekly-change', checkSectionIdCreator, (req, res) => {
	let { weeklyUid, dayOfTheWeek, time, canceled, sectionUid } = req.body;
	SectionDB.weeklyChangeAction(dayOfTheWeek, time, canceled, weeklyUid, sectionUid, 'Add', status => res.json({ code: status ? 0 : -1 }));
});

router.post('/remove-weekly-change', checkSectionIdCreator, (req, res) => {
	let { weeklyUid, sectionUid } = req.body;
	SectionDB.weeklyChangeAction(null, null, null, weeklyUid, sectionUid, 'Delete', status => res.json({ code: status ? 0 : -1 }));
});

router.post('/get-events', (req, res) => {
	let { uid, page } = req.body;
	page--;

	SectionDB.getSectionById(uid, section => {
		if (!section) return res.json({ code: 1 });
		if (!section.calendar) return res.json({ code: 0, events: [] });
		if (!section.calendar.events) return res.json({ code: 0, events: [] });
		if (section.calendar.events.length == 0) return res.json({ code: 0, events: [] });

		let events = section.calendar.events.sort((a, b) => Date.parse(b.time) - Date.parse(a.time)).map(e => {
			if (!e.members) e.members = [];
			e.coming = e.members.map(m => m.toString()).indexOf(req.user._id.toString()) != -1;
			return e;
		});
		let count = events.length;

		let firstIndex = pageSize * page;
		let lastIndex = firstIndex + pageSize;
		let more = lastIndex < events.length;

		if (firstIndex > events.length) return res.json({ code: 0, events: [] });

		events = events.slice(firstIndex, lastIndex > events.length ? events.length : lastIndex);

		res.json({ code: 0, events, more });
	});
});

router.post('/get-past-events', (req, res) => {
	let { uid, page } = req.body;
	page--;

	SectionDB.getSectionById(uid, section => {
		if (!section) return res.json({ code: 1 });
		if (!section.calendar) return res.json({ code: 0, events: [] });
		if (!section.calendar.events) return res.json({ code: 0, events: [] });
		if (section.calendar.events.length == 0) return res.json({ code: 0, events: [] });

		let events = section.calendar.events
			.filter(e => Date.parse(e.time) < new Date())
			.sort((a, b) => Date.parse(b.time) - Date.parse(a.time))
			.map(e => {
				if (!e.members) e.members = [];
				e.coming = e.members.map(m => m.toString()).indexOf(req.user._id.toString()) != -1;
				return e;
			});
		let count = events.length;

		let firstIndex = pageSize * page;
		let lastIndex = firstIndex + pageSize;
		let more = lastIndex < events.length;

		if (firstIndex > events.length) return res.json({ code: 0, events: [] });

		events = events.slice(firstIndex, lastIndex > events.length ? events.length : lastIndex);

		res.json({ code: 0, events, more });
	});
});

router.post('/get-next-events', (req, res) => {
	let { uid, page } = req.body;
	page--;

	SectionDB.getSectionById(uid, section => {
		if (!section) return res.json({ code: 1 });
		if (!section.calendar) return res.json({ code: 0, events: [] });
		if (!section.calendar.events) return res.json({ code: 0, events: [] });
		if (section.calendar.events.length == 0) return res.json({ code: 0, events: [] });

		let events = section.calendar.events
			.filter(e => Date.parse(e.time) >= new Date())
			.sort((a, b) => Date.parse(a.time) - Date.parse(b.time))
			.map(e => {
				if (!e.members) e.members = [];
				e.coming = e.members.map(m => m.toString()).indexOf(req.user._id.toString()) != -1;
				return e;
			});
		let count = events.length;

		let firstIndex = pageSize * page;
		let lastIndex = firstIndex + pageSize;
		let more = lastIndex < events.length;

		if (firstIndex > events.length) return res.json({ code: 0, events: [] });

		events = events.slice(firstIndex, lastIndex > events.length ? events.length : lastIndex);

		res.json({ code: 0, events, more });
	});
});

router.post('/new-event', checkSectionIdCreator, (req, res) => {
	let { name, time, location, text, required, sectionUid } = req.body;
	SectionDB.addEvent(name, time, location, text, required, sectionUid, eventUid => res.json(!eventUid ? { code: -1 } : { code: 0, eventUid }));
});

router.post('/remove-event', checkSectionIdCreator, (req, res) => {
	let { eventUid, sectionUid } = req.body;
	SectionDB.removeEvent(eventUid, sectionUid, status => res.json({ code: status ? 0 : -1 }));
});

router.post('/coming-to-event', (req, res) => {
	let { userUid, sectionUid, eventUid, coming } = req.body;
	if (userUid != req.user._id) return res.json({ code: -3 });
	SectionDB.confirmEvent(userUid, sectionUid, eventUid, coming, status => res.json({ code: status ? 0 : -1 }));
});

router.post('/get-notifications', (req, res) => {
	let { uid, page } = req.body;
	page--;

	SectionDB.getSectionById(uid, section => {
		if (!section) return res.json({ code: 1 });

		if (!section.notifications) return res.json({ code: 0, notifications: [] });
		if (section.notifications.length == 0) return res.json({ code: 0, notifications: [] });

		let notifications = section.notifications.sort((a, b) => Date.parse(b.time) - Date.parse(a.time));
		let count = notifications.length;

		let firstIndex = pageSize * page;
		let lastIndex = firstIndex + pageSize;
		let more = lastIndex < notifications.length;

		if (firstIndex > notifications.length) return res.json({ code: 0, notifications: [] });

		notifications = notifications.slice(firstIndex, lastIndex > notifications.length ? notifications.length : lastIndex);

		res.json({ code: 0, notifications, more });
	});
});

router.post('/new-notification', checkSectionIdCreator, (req, res) => {
	let { title, text, sectionUid } = req.body;
	SectionDB.notificationAction(title, text, sectionUid, null, 'Add', notificationUid => res.json(!notificationUid ? { code: -1 } : { code: 0, notificationUid }));
});

router.post('/remove-notification', checkSectionIdCreator, (req, res) => {
	let { notificationUid, sectionUid } = req.body;
	SectionDB.notificationAction(null, null, sectionUid, notificationUid, 'Delete', status => res.json({ code: status ? 0 : -1 }));
});

router.post('/search-sections', (req, res) => {
	let { query } = req.body;

	SectionDB.searchSections(query, sections => {
		if (!sections) return res.json({ code: 1 });
		res.json({ code: 0, sections });
	});
});

router.post(
	'/edit-section-info',
	(req, res, next) => {
		SectionDB.getSectionById(req.body.uid, section => {
			if (section.creator.toString() != req.user._id.toString()) return res.json({ code: -3 });
			next();
		});
	},
	(req, res) => {
		let { uid, name, description, location, acceptingMembers } = req.body;
		SectionDB.editSection(uid, name, description, location, acceptingMembers, status => res.json({ code: status ? 0 : 1 }));
	}
);
