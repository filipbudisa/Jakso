import express from 'express';
import crypto from 'crypto';
import passport from 'passport';
import { Strategy } from 'passport-local';

import * as UserDB from './userDB';

const router = express.Router();
export default router;

const config = { usernameField: 'email', passwordField: 'password', passReqToCallback: true };
passport.serializeUser((user, done) => done(null, user._id));
passport.deserializeUser((id, done) => UserDB.getUserById(id, user => done(null, user)));
passport.use(
	'local-login',
	new Strategy(config, (req, email, password, done) => {
		password = crypto
			.createHash('sha256')
			.update(password)
			.digest('base64');

		UserDB.getUserByEmailAndPassword(email, password, user => done(null, user));
	})
);

const authenticate = (req, res) => {
	passport.authenticate('local-login', (err, user, info) => {
		if (err || !user) return res.json({ code: 1 });
		req.logIn(user, err => {
			if (err) return res.json({ code: 1 });
			res.json({ code: 0, user });
		});
	})(req, res);
};

router.post('/register', (req, res) => {
	let { email, password, name, surname, birthDate } = req.body;

	password = crypto
		.createHash('sha256')
		.update(password)
		.digest('base64');

	UserDB.getUserByEmail(email, user => {
		if (user) return res.json({ code: 6 });
		UserDB.newUser(email, password, name, surname, birthDate, status => {
			if (!status) return res.json({ code: 3 });
			authenticate(req, res);
		});
	});
});

router.post('/login', authenticate);
router.get('/logout', (req, res) => req.logout());
