import express from 'express';

import * as UserDB from './userDB';

const router = express.Router();
export default router;

router.post('/overview', (req, res) => {
	let { uid } = req.body;
	UserDB.getUserByIdIncludeSection(uid, user => {
		if (!user) return res.json({ code: 1 });

		if (!user.joinedSections) user.joinedSections = [];
		user.joinedSections = user.joinedSections.map(section => {
			let { uid, name, notifications, calendar } = section;
			if (!notifications) notifications = [];
			if (!calendar) calendar = {};
			if (!calendar.events) calendar.events = [];

			let notification = notifications.length == 0 ? {} : notifications.sort((a, b) => Date.parse(b.time) - Date.parse(a.time))[0];
			let event =
				calendar.events.length == 0
					? {}
					: calendar.events
							.filter(_event => {
								if (_event.required) return true;
								if (_event.members && _event.members.map(m => m.toString()).indexOf(user._id.toString()) != -1) return true;
								return false;
							})
							.sort((a, b) => Date.parse(b.time) - Date.parse(a.time))[0];

			return { uid, name, notification, event };
		});

		res.json({ code: 0, user });
	});
});

router.post('/get-user', (req, res) => {
	let { uid } = req.body;

	UserDB.getUserByIdIncludeSection(uid, user => {
		if (!user) return res.json({ code: 1 });

		if (!user.joinedSections) user.joinedSections = [];
		user.joinedSections = user.joinedSections.map(section => ({ uid: section._id, name: section.name }));

		res.json({ code: 0, user });
	});
});

router.post('/edit-user-info', (req, res) => {
	let { uid, email, name, surname, birthDate } = req.body;
	if (uid.toString() != res.user._id.toString()) return res.json({ code: -3 });
	UserDB.editUser(uid, email, name, surname, birthDate, status => res.json({ code: status ? 0 : 1 }));
});
