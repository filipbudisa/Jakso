import mongoose, { mongo } from 'mongoose';
import { User, Section } from './models';
import { mongodb } from '../properties';

export const getUserByEmailAndPassword = (email, password, callback) => {
	mongoose.connect(mongodb, () => {
		User.find({ email, password }, (err, users) => {
			let user = users.length == 0 ? null : users[0];
			if (!user) return callback(null);
			user.uid = user._id;
			callback(user);
		});
	});
};

export const getUserByEmail = (email, callback) => {
	mongoose.connect(mongodb, () => {
		User.find({ email }, (err, users) => {
			let user = users.length == 0 ? null : users[0];
			if (!user) return callback(null);
			user.uid = user._id;
			callback(user);
		});
	});
};

export const getUserById = (id, callback) => {
	mongoose.connect(mongodb, () => {
		User.find({ _id: id }, (err, users) => {
			let user = users.length == 0 ? null : users[0];
			if (!user) return callback(null);
			user.uid = user._id;
			callback(user);
		});
	});
};

export const getUserByEmailAndPasswordIncludeSection = (email, password, callback) => {
	mongoose.connect(mongodb, () => {
		User.find({ email, password }, (err, users) => {
			let user = users.length == 0 ? null : users[0];
			if (!user) return callback(null);
			if (!user.joinedSections) user.joinedSections = [];
			user.uid = user._id;

			Section.find({ _id: user.joinedSections }, (err, sections) => {
				sections = sections.map(section => {
					section.uid = section._id;
					return section;
				});

				user = Object.assign({}, user._doc, { joinedSections: sections ? sections : [] });
				callback(user);
			});
		});
	});
};

export const getUserByIdIncludeSection = (id, callback) => {
	mongoose.connect(mongodb, () => {
		User.find({ _id: id }, (err, users) => {
			let user = users.length == 0 ? null : users[0];
			if (!user) return callback(null);
			if (!user.joinedSections) user.joinedSections = [];
			user.uid = user._id;

			Section.find({ _id: user.joinedSections }, (err, sections) => {
				sections = sections.map(section => {
					section.uid = section._id;
					return section;
				});

				user = Object.assign({}, user._doc, { joinedSections: sections ? sections : [] });
				callback(user);
			});
		});
	});
};

export const newUser = (email, password, name, surname, birthDate, callback) => {
	mongoose.connect(mongodb, () => {
		User.find({ email }, (err, users) => {
			if (users.length != 0) return callback(null);

			let user = new User({ email, password, name, surname, birthDate });
			user.save((err, product) => {
				if (err) return callback(null);
				callback(true);
			});
		});
	});
};

export const editUser = (id, email, name, surname, birthDate, callback) => {
	mongoose.connect(mongodb, () => {
		User.find({ _id: id }, (err, users) => {
			if (users.length == 0) return callback(null);

			let changes = {};
			if (email) Object.assign(changes, { email });
			if (name) Object.assign(changes, { name });
			if (surname) Object.assign(changes, { surname });
			if (birthDate) Object.assign(changes, { birthDate });

			User.update({ _id: id }, changes, (err, n, raw) => {
				if (err) return callback(null);
				return callback(true);
			});
		});
	});
};
