import {Component, OnInit} from '@angular/core';
import {SessionService} from './session.service';
import {Router} from '@angular/router';
import {BackendService} from './backend.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
	title = 'Jakso';

	constructor(public session: SessionService, public backend: BackendService, public router: Router) {
	}


	ngOnInit(): void {
	}

	recover(){
		this.router.navigate(["/"]);
	}
}
