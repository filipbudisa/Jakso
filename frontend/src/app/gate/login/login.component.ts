import {Component, OnInit} from '@angular/core';
import {BackendService} from '../../backend.service';
import {Returnable} from '../../dataTypes/returnable.interface';
import {SessionService} from '../../session.service';
import {Router} from '@angular/router';
import {Message} from '../../templateClasses/message';
import {LoginResult} from '../../dataTypes/results/loginResult.interface';
import {DataStoreService} from '../../data-store.service';

@Component({
	selector: 'app-root',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent extends Message implements OnInit {
	username: string;
	password: string;

	constructor(private backend: BackendService, private session: SessionService, private router: Router, private dataS: DataStoreService) {
		super();
	}

	ngOnInit() {
		if(this.dataS.accCreated){
			this.dataS.accCreated = false;
			this.printMessage("Korisnički račun uspješno izrađen");
		}else{
			this.printMessage(); // za dobru meru
		}
	}

	tryLogin(){
		this.printMessage();

		this.backend.login(this.username, this.password).subscribe(event => {
			let data: LoginResult = event;

			if(data.code == 0){
				this.session.setSession(data.user.uid);
				this.dataS.updateSections();
				this.router.navigate(["/"]);
			}else if(data.code == -1){
				this.printMessage(data.error);
			}else{
				this.printMessage(BackendService.getMessage(data));
			}
		});
	}
}
