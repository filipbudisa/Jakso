import {NgModule} from '@angular/core';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {FormsModule} from '@angular/forms';
import {AppRouterModule} from '../app-router.module';
import {CommonModule} from '@angular/common';

@NgModule({
	imports: [
		FormsModule,
		CommonModule,
		AppRouterModule
	],
	declarations: [LoginComponent, RegisterComponent],
	exports: [LoginComponent, RegisterComponent]
})
export class GateModule {
}
