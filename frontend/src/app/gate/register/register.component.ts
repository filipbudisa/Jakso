import {Component, OnInit} from '@angular/core';
import {Message} from '../../templateClasses/message';
import {BackendService} from '../../backend.service';
import {Returnable} from '../../dataTypes/returnable.interface';
import {SessionService} from '../../session.service';
import {Router} from '@angular/router';
import {DataStoreService} from '../../data-store.service';

@Component({
	selector: 'app-root',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.css']
})
export class RegisterComponent extends Message implements OnInit {
	pass: string = "";
	pass2: string = "";
	email: string = "";
	ime: string = "";
	prezime: string = "";
	dob: string = "";

	constructor(private backend: BackendService, private session: SessionService, private router: Router, private data: DataStoreService) {
		super();
	}

	ngOnInit() {
	}

	tryRegister(): void {
		this.printMessage();

		if(this.pass != this.pass2){
			this.printMessage("Passwords do not match");
			return;
		}

		if(this.email == "" || this.pass == ""){
			this.printMessage("All fields are required");
			return;
		}

		this.backend.register(this.email, this.pass, this.ime, this.prezime, this.dob).subscribe(event => {
			let data: Returnable = event;

			if(data.code == 0){
				this.email = this.pass = this.pass2 = this.ime = this.prezime = this.dob = "";
				this.printMessage();
				this.data.accCreated = true;
				this.router.navigate(["/"]);
			}else if(data.code == -1){
				this.printMessage(data.error);
			}else{
				this.printMessage(BackendService.getMessage(data));
			}
		});
	}
}
