import {AfterViewInit, Component, Input, OnChanges, OnInit} from '@angular/core';
import {SekcijaResult} from '../../../dataTypes/results/sekcijaResult.interface';
import {Section} from '../../../dataTypes/model/section.interface';
import {SekcijaComponent} from '../sekcija.component';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';
import {Application} from '../../../dataTypes/model/application.interface';

@Component({
	selector: 'app-sekcija-outside',
	templateUrl: './sekcija-outside.component.html',
	styleUrls: ['./sekcija-outside.component.css']
})
export class SekcijaOutsideComponent implements OnInit, OnChanges {

	@Input() sekcijaResult: SekcijaResult;
	@Input() sekcijaUid: string;
	@Input() application: Application;

	sekcija: Section = <Section> { name: "", location: "", description: "", acceptingMembers: false };

	constructor(private router: Router) {
	}

	ngOnInit() {
	}

	ngOnChanges(){
		this.loadSekcija(this.sekcijaResult);
	}

	loadSekcija(result: SekcijaResult){
		this.sekcija = result.section;
	}

	posaljiPrijavu(){
		this.router.navigate(["/prijava", "nova", this.sekcijaResult.section.uid]);
	}
}
