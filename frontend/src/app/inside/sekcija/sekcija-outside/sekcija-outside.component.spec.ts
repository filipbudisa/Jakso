import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SekcijaOutsideComponent } from './sekcija-outside.component';

describe('SekcijaOutsideComponent', () => {
  let component: SekcijaOutsideComponent;
  let fixture: ComponentFixture<SekcijaOutsideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SekcijaOutsideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SekcijaOutsideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
