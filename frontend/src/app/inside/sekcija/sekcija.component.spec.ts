import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SekcijaComponent } from './sekcija.component';

describe('SekcijaComponent', () => {
  let component: SekcijaComponent;
  let fixture: ComponentFixture<SekcijaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SekcijaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SekcijaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
