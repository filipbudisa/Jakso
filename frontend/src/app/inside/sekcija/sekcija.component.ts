import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Notification} from 'app/dataTypes/model/notification.interface'
import {Event} from 'app/dataTypes/model/event.interface'
import {ActivatedRoute} from '@angular/router';
import {Section} from '../../dataTypes/model/section.interface';
import {SekcijaResult} from '../../dataTypes/results/sekcijaResult.interface';
import {WeeklyChange} from '../../dataTypes/model/weekly-change.interface';
import {BackendService} from '../../backend.service';
import {SessionService} from '../../session.service';
import {SekcijaInsideComponent} from './sekcija-inside/sekcija-inside.component';
import {SekcijaOutsideComponent} from './sekcija-outside/sekcija-outside.component';
import {Observable} from 'rxjs/Observable';
import {Application} from '../../dataTypes/model/application.interface';

@Component({
	selector: 'app-sekcija',
	templateUrl: './sekcija.component.html',
	styleUrls: ['./sekcija.component.css']
})
export class SekcijaComponent implements OnInit {
	sectionMember: boolean;

	sekcijaUid: string;
	sekcijaResult: SekcijaResult;

	application: Application = undefined;

	constructor(private route: ActivatedRoute, private backend: BackendService, private session: SessionService) {

	}

	ngOnInit() {
		this.route.paramMap.subscribe(params => {
			this.sectionMember = false;
			this.sekcijaResult = <SekcijaResult> { section: { name: "" } };

			this.sekcijaUid = params.get("uid");

			this.sectionMember = undefined;
			this.application = undefined;
			this.loadSekcija();
		});
	}

	loadSekcija() {
		this.backend.getSection(this.sekcijaUid).subscribe((result: SekcijaResult) => {
			this.sekcijaResult = result;

			let uuid = this.session.getUid();
			this.sectionMember = uuid == result.section.creator || (result.section.members.indexOf(uuid) != -1);

			if(!result.isMember && result.hasApplication){
				this.application = result.application;
			}
		});
	}

}
