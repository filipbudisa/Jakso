import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VanredniTerminaComponent } from './vanredni-termina.component';

describe('VanredniTerminaComponent', () => {
  let component: VanredniTerminaComponent;
  let fixture: ComponentFixture<VanredniTerminaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VanredniTerminaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VanredniTerminaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
