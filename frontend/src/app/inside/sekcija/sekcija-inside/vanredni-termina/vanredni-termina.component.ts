import {Component, Input, OnInit} from '@angular/core';
import {SekcijaInsideComponent} from '../sekcija-inside.component';
import {WeeklyChange} from '../../../../dataTypes/model/weekly-change.interface';
import {Message} from '../../../../templateClasses/message';
import {BackendService} from '../../../../backend.service';
import {Returnable} from '../../../../dataTypes/returnable.interface';

@Component({
	selector: 'app-vanredni-termina',
	templateUrl: './vanredni-termina.component.html',
	styleUrls: ['./vanredni-termina.component.css']
})
export class VanredniTerminaComponent extends Message implements OnInit {
	@Input() sekcijaInside: SekcijaInsideComponent;

	wcWeekly: string = "";
	wcDow: number = 0;
	wcHr: number = undefined;
	wcMin: number = undefined;
	wcOtkazano: boolean = false;

	constructor(private backend: BackendService) {
		super();
	}

	ngOnInit() {
	}

	makniTermin(wc: WeeklyChange){
		this.printMessage();

		this.backend.removeWeeklyChange(wc.uid, this.sekcijaInside.sekcijaUid).subscribe((res: Returnable) => {
			if(res.code == 0){
				this.sekcijaInside.weeklyChanges.splice(this.sekcijaInside.weeklyChanges.indexOf(wc), 1);
			}else if(res.code != -1){
				this.printMessage(BackendService.getMessage(res));
			}
		});
	}

	dodajTermin(){
		this.printMessage();

		if(this.wcWeekly == undefined || this.wcWeekly == ""){
			this.printMessage("Odaberite termin");
			return;
		}

		if(!this.wcOtkazano && (this.wcHr == undefined || this.wcMin == undefined)){
			this.printMessage("Vrijeme je obavezno");
			return;
		}

		this.backend.newWeeklyChange(this.wcWeekly, this.sekcijaInside.sekcijaUid, this.wcOtkazano, this.wcDow,
			"" + this.wcHr + ":" + this.wcMin).subscribe((res: Returnable) => {

			if(res.code == 0){
				this.sekcijaInside.weeklyChanges.push(<WeeklyChange> {
					uid: this.wcWeekly,
					canceled: this.wcOtkazano,
					dow: this.wcDow,
					time: "" + this.wcHr + ":" + this.wcMin,
					parent: this.sekcijaInside.weeklies.find(w => w.uid == this.wcWeekly),
					class: this.wcOtkazano ? "termin termin_otk" : "termin termin_pomak simple"
				});
			}else if(res.code != -1){
				this.printMessage(BackendService.getMessage(res));
			}
		});
	}

	ugasi(){
		this.sekcijaInside.ugasiVanredne();
	}
}
