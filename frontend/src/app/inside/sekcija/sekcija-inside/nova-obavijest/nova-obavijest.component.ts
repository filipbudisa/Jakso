import {Component, HostBinding, Input, OnInit} from '@angular/core';
import {SekcijaInsideComponent} from '../sekcija-inside.component';
import {Message} from '../../../../templateClasses/message';
import {BackendService} from '../../../../backend.service';
import {Returnable} from '../../../../dataTypes/returnable.interface';
import {Notification} from '../../../../dataTypes/model/notification.interface';
import {NewNotificationResult} from '../../../../dataTypes/results/newNotificationResult.interface';

@Component({
	selector: 'app-nova-obavijest',
	templateUrl: './nova-obavijest.component.html',
	styleUrls: ['./nova-obavijest.component.css']
})
export class NovaObavijestComponent extends Message implements OnInit {
	title: string = "";
	text: string = "";

	@Input() sekcijaInside: SekcijaInsideComponent;

	constructor(private backend: BackendService) {
		super();
	}

	ngOnInit() {
	}

	novaObavijest(){
		this.printMessage();

		if(this.title == "" || this.text == ""){
			this.printMessage("Nije moguće objaviti obavijest bez naslova ili teksta.");
			return;
		}

		this.backend.newNotification(this.sekcijaInside.sekcijaUid, this.title, this.text).subscribe((res: NewNotificationResult) => {
			if(res.code == 0){
				this.sekcijaInside.notifications.unshift(<Notification> {
					title: this.title,
					text: this.text,
					time: (new Date()).toISOString(),
					uid: res.notificationUid
				});

				this.title = "";
				this.text = "";
				this.sekcijaInside.ugasiObavijest();
			}else if(res.code != -1){
				this.printMessage(BackendService.getMessage(res));
			}
		});
	}

	odustani(){
		this.title = "";
		this.text = "";
		this.sekcijaInside.ugasiObavijest();
	}
}
