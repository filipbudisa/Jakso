import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NovaObavijestComponent } from './nova-obavijest.component';

describe('NovaObavijestComponent', () => {
  let component: NovaObavijestComponent;
  let fixture: ComponentFixture<NovaObavijestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovaObavijestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovaObavijestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
