import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SekcijaInsideComponent } from './sekcija-inside.component';

describe('SekcijaInsideComponent', () => {
  let component: SekcijaInsideComponent;
  let fixture: ComponentFixture<SekcijaInsideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SekcijaInsideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SekcijaInsideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
