import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DogadajDolazniciComponent } from './dogadaj-dolaznici.component';

describe('DogadajDolazniciComponent', () => {
  let component: DogadajDolazniciComponent;
  let fixture: ComponentFixture<DogadajDolazniciComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DogadajDolazniciComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DogadajDolazniciComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
