import {Component, Input, OnInit} from '@angular/core';
import {SekcijaInsideComponent} from '../sekcija-inside.component';
import {Event} from '../../../../dataTypes/model/event.interface';

@Component({
	selector: 'app-dogadaj-dolaznici',
	templateUrl: './dogadaj-dolaznici.component.html',
	styleUrls: ['./dogadaj-dolaznici.component.css']
})
export class DogadajDolazniciComponent implements OnInit {
	@Input() sekcijaInside: SekcijaInsideComponent;
	@Input() event: Event = { section: null, uid: "", name: "", text: "", time: "", location: "", required: false };
	@Input() dolaznici: { name: string, surname: string }[] = [];

	constructor() {
	}

	ngOnInit() {
	}

	ugasi(){
		this.sekcijaInside.ugasiDolaznici();
	}

}
