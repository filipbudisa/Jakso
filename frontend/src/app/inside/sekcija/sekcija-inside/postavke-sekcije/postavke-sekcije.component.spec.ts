import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostavkeSekcijeComponent } from './postavke-sekcije.component';

describe('PostavkeSekcijeComponent', () => {
  let component: PostavkeSekcijeComponent;
  let fixture: ComponentFixture<PostavkeSekcijeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostavkeSekcijeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostavkeSekcijeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
