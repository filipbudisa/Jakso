import {Component, Input, OnInit} from '@angular/core';
import {SekcijaInsideComponent} from '../sekcija-inside.component';
import {Message} from '../../../../templateClasses/message';
import {BackendService} from '../../../../backend.service';
import {Returnable} from '../../../../dataTypes/returnable.interface';

@Component({
	selector: 'app-postavke-sekcije',
	templateUrl: './postavke-sekcije.component.html',
	styleUrls: ['./postavke-sekcije.component.css']
})
export class PostavkeSekcijeComponent extends Message implements OnInit {
	name: string = "";
	description: string = "";
	location: string = "";
	accepting: boolean = false;

	@Input() sekcijaInside: SekcijaInsideComponent;

	constructor(private backend: BackendService) {
		super();
	}

	ngOnInit() {
		this.name = this.sekcijaInside.sekcija.name;
		this.location = this.sekcijaInside.sekcija.location;
		this.accepting = this.sekcijaInside.sekcija.acceptingMembers;
		this.description = this.sekcijaInside.sekcija.description;
	}

	spremi(){
		this.printMessage();

		if(this.name == "" || this.description == "" || this.location == ""){
			this.printMessage("Sva polja su obavezna!");
			return;
		}

		this.backend.editSection(this.sekcijaInside.sekcijaUid, this.name, this.description,
			this.location, this.accepting).subscribe((res: Returnable) => {

			if(res.code == 0){
				this.sekcijaInside.sekcija.location = this.location;
				this.sekcijaInside.sekcija.name = this.name;
				this.sekcijaInside.sekcija.description = this.description;

				this.sekcijaInside.ugasiPostavke();
			}else if(res.code != -1){
				this.printMessage(BackendService.getMessage(res));
			}
		});
	}

	odustani(){
		this.sekcijaInside.ugasiPostavke();
	}

}
