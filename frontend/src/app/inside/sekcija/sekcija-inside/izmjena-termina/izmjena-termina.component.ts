import {Component, Input, OnInit} from '@angular/core';
import {SekcijaInsideComponent} from '../sekcija-inside.component';
import {Weekly} from '../../../../dataTypes/model/weekly.interface';
import {BackendService} from '../../../../backend.service';
import {Message} from '../../../../templateClasses/message';
import {Returnable} from '../../../../dataTypes/returnable.interface';

@Component({
	selector: 'app-izmjena-termina',
	templateUrl: './izmjena-termina.component.html',
	styleUrls: ['./izmjena-termina.component.css']
})
export class IzmjenaTerminaComponent extends Message implements OnInit {
	@Input() sekcijaInside: SekcijaInsideComponent;

	tdDow: number = 0;
	tdTimeHr: number = 0;
	tdTimeMin: number = 0;

	constructor(private backend: BackendService) {
		super();
	}


	ngOnInit() {
	}

	ugasi(){
		this.tdDow = 0;
		this.tdTimeHr = 0;
		this.tdTimeMin = 0;
		this.sekcijaInside.ugasiTermin();
	}

	makniTermin(weekly: Weekly){
		this.printMessage();

		this.backend.removeWeekly(this.sekcijaInside.sekcijaUid, weekly.uid).subscribe((res: Returnable) => {
			if(res.code == 0){
				this.sekcijaInside.weeklies.splice(this.sekcijaInside.weeklies.indexOf(weekly), 1);
			}else if(res.code != -1){
				this.printMessage(BackendService.getMessage(res));
			}
		});
	}

	dodajTermin(){
		this.printMessage();

		if(this.tdTimeHr == undefined || this.tdTimeMin == undefined){
			this.printMessage("Vrijeme je obavezno");
			return;
		}

		this.backend.newWeekly(this.sekcijaInside.sekcijaUid, this.tdDow,
			"" + this.tdTimeHr + ":" + this.tdTimeMin).subscribe((res: Returnable & { weeklyUid: string }) => {

			if(res.code == 0){
				this.sekcijaInside.weeklies.push(<Weekly> {
					dow: this.tdDow,
					time: "" + this.tdTimeHr + ":" + this.tdTimeMin,
					uid: res.weeklyUid
				});
			}else if(res.code != -1){
				this.printMessage(BackendService.getMessage(res));
			}
		});
	}

}
