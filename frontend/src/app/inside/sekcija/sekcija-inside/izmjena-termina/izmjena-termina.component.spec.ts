import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IzmjenaTerminaComponent } from './izmjena-termina.component';

describe('IzmjenaTerminaComponent', () => {
  let component: IzmjenaTerminaComponent;
  let fixture: ComponentFixture<IzmjenaTerminaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IzmjenaTerminaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IzmjenaTerminaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
