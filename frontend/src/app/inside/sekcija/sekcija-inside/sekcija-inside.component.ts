import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {Notification} from '../../../dataTypes/model/notification.interface';
import {Event} from '../../../dataTypes/model/event.interface';
import {ActivatedRoute, Router} from '@angular/router';
import {Section} from '../../../dataTypes/model/section.interface';
import {SessionService} from '../../../session.service';
import {Weekly} from '../../../dataTypes/model/weekly.interface';
import {WeeklyChange} from '../../../dataTypes/model/weekly-change.interface';
import {BackendService} from '../../../backend.service';
import {SekcijaResult} from '../../../dataTypes/results/sekcijaResult.interface';
import {NotificationsResult} from '../../../dataTypes/results/notificationsResult.interface';
import {EventsResult} from '../../../dataTypes/results/eventsResult.interface';
import {Returnable} from '../../../dataTypes/returnable.interface';
import {UserResult} from '../../../dataTypes/results/userResult.interface';
import {forEach} from '@angular/router/src/utils/collection';
import {Application} from '../../../dataTypes/model/application.interface';
import {ApplicationResult} from '../../../dataTypes/results/applicationResult.interface';
import {ApplicationsResult} from '../../../dataTypes/results/applicationsResult.interface';
import * as moment from 'moment';



@Component({
	selector: 'app-sekcija-inside',
	templateUrl: './sekcija-inside.component.html',
	styleUrls: ['./sekcija-inside.component.css']
})
export class SekcijaInsideComponent implements OnInit, OnChanges {
	novaObavijestShown: boolean = false;
	noviDogadajShown: boolean = false;
	terminiShown: boolean = false;
	vanredniShow: boolean = false;
	postavkeShown: boolean = false;
	dolazniciShow: boolean = false;
	prijaveShown: boolean = false;

	notifications: Notification[] = [];
	events: Event[] = [];
	weeklies: Weekly[] = [];
	weeklyChanges: WeeklyChange[] = [];

	@Input() sekcijaResult: SekcijaResult;
	@Input() sekcijaUid: string;
	sekcija: Section = <Section> { uid: "", name: "", location: "", description: "", creator: "", };

	owner: boolean;

	osnivac: { name: string, surname: string, email: string } = { name: "", surname: "", email: "" };
	clanovi: { uid: string, name: string, surname: string }[] = [];

	dolaznici: { name: string, surname: string }[] = [];
	dolazniciEvent: Event = { section: this.sekcija, uid: "", name: "", text: "", time: "", location: "", required: false };

	prijave: (Application & {
		sectionName: string,
		userName: string,
		status: number
	})[] = [];

	imaJosObavijesti: boolean = false;
	imaJosDogadaja: boolean = true;
	obaPage: number = 1;
	dogPage: number = 1;

	public moment: any = moment;

	readonly days: string[] = [
		"Ponedjeljak", "Utorak", "Srijeda", "Četvrtak", "Petak", "Subota", "Nedjelja"
	];

	constructor(private router: Router, private route: ActivatedRoute, private session: SessionService, private backend: BackendService) {
	}

	ngOnInit() {
	}

	ngOnChanges(){
		this.loadSekcija(this.sekcijaResult);
	}

	loadSekcija(result: SekcijaResult){
		console.log("Učitavam sekciju " + this.sekcijaUid);

		this.notifications = [];
		this.events = [];
		this.osnivac = { name: "", surname: "", email: "" };
		this.clanovi = [];

		/** ## */
		this.sekcija = result.section;
		this.owner = result.section.creator == this.session.getUid();

		if(result.section.calendar !== undefined){
			if(result.section.calendar.regularWeekly !== undefined)
				this.weeklies = result.section.calendar.regularWeekly;
			if(result.section.calendar.regularWeeklyChange !== undefined)
				this.weeklyChanges = result.section.calendar.regularWeeklyChange;
		}

		this.weeklyChanges.forEach((wc: WeeklyChange) => {
			wc.parent = this.weeklies.find(w => w.uid == wc.uid);
			console.log();
			wc.class = wc.canceled ? "termin termin_otk" : "termin termin_pomak simple";
		});
		/** ## */

		this.backend.getUser(this.sekcija.creator).subscribe((result: UserResult) => {
			this.osnivac = result.user;
		});

		this.sekcija.members.forEach(m => {
			this.backend.getUser(m).subscribe((result: UserResult) => {
				this.clanovi.push(result.user);
			});
		});

		this.backend.getNotifications(this.sekcijaUid, 1).subscribe((result: NotificationsResult) => {
			this.notifications = this.notifications.concat(result.notifications);
			this.imaJosObavijesti = result.more;
			this.obaPage = 1;
		});

		this.backend.getEvents(this.sekcijaUid, 1).subscribe((result: EventsResult) => {
			this.events = this.events.concat(result.events);
			this.imaJosDogadaja = result.more;
			this.dogPage = 1;
		});

		this.backend.getApplications(this.sekcijaUid).subscribe((result: ApplicationsResult) => {
			this.prijave = result.applications;
		});
	}

	josObavijesti(){
		this.backend.getNotifications(this.sekcijaUid, this.obaPage+1).subscribe((result: NotificationsResult) => {
			this.notifications = this.notifications.concat(result.notifications);
			this.imaJosObavijesti = result.more;
			this.obaPage++;
		});
	}

	josDogadaja(){
		this.backend.getEvents(this.sekcijaUid, this.dogPage+1).subscribe((result: EventsResult) => {
			this.events = this.events.concat(result.events);
			this.imaJosDogadaja = result.more;
			this.dogPage++;
		});
	}

	eventComing(event: Event, coming: boolean){
		if(event.coming == coming) return;

		event.coming = undefined;

		this.backend.eventRsvp(this.sekcijaUid, event.uid, this.session.getUid(), coming).subscribe((res: Returnable) => {
			if(res.code == 0) event.coming = coming;
			else alert(BackendService.getMessage(res));
		})
	}

	novaObavijest() {
		this.novaObavijestShown = true;
	}

	noviDogadaj() {
		this.noviDogadajShown = true;
	}

	ugasiObavijest() {
		this.novaObavijestShown = false;
	}

	ugasiDogadaj() {
		this.noviDogadajShown = false;
	}

	izmjenaTermina() {
		this.terminiShown = true;
	}

	ugasiTermin() {
		this.terminiShown = false;
	}

	vanredniTermina(){
		this.vanredniShow = true;
	}

	ugasiVanredne(){
		this.vanredniShow = false;
	}

	postavkeSekcije() {
		this.postavkeShown = true;
	}

	ugasiPostavke(){
		this.postavkeShown = false;
	}

	otvoriPrijave(){
		console.log("otvaram");
		this.prijaveShown = true;
	}

	ugasiPrijave(){
		this.prijaveShown = false;
	}

	eventDolaznici(event: Event){
		this.dolazniciShow = true;

		this.dolaznici = [];
		this.dolazniciEvent = event;

		this.backend.getEvents(this.sekcijaUid, 1).subscribe((result: EventsResult) => {
			result.events.find(e => e.uid == event.uid).members.forEach(d => {
				this.backend.getUser(d).subscribe((res: UserResult) => {
					this.dolaznici.push(res.user);
				});
			});
		});
	}

	ugasiDolaznici(){
		this.dolazniciShow = false;
	}

	makniObavijest(obavijest: Notification){
		if(confirm("Ukloni obavijest " + obavijest.title + "?")){
			this.backend.removeNotification(this.sekcijaUid, obavijest.uid).subscribe((res: Returnable) => {
				this.notifications.splice(this.notifications.indexOf(obavijest), 1);
			});
		}
	}

	makniEvent(event: Event){
		if(confirm("Ukloni događaj " + event.name+ "?")){
			this.backend.removeEvent(this.sekcijaUid, event.uid).subscribe((res: Returnable) => {
				this.events.splice(this.events.indexOf(event), 1);
			});
		}
	}

	makniClana(clan: { uid: string, name: string, surname: string }){
		if(confirm("Izbaci " + clan.name + " " + clan.surname + " iz sekcije?")){
			this.backend.removeMember(this.sekcijaUid, clan.uid).subscribe((res: Returnable) => {
				if(res.code == 0){
					this.clanovi.slice(this.clanovi.findIndex(c => c.uid == clan.uid), 1);
				}else{
					alert(BackendService.getMessage(res));
				}
			});
		}
	}

	obrisiSekciju(){
		if(confirm("Obriši seksiju?")){
			this.backend.deleteSection(this.sekcijaUid).subscribe((res) => {
				if(res.code == 0){
					this.router.navigate(["/"]);
				}else{
					alert(BackendService.getMessage(res));
				}
			});
		}
	}

	napustiSekciju(){
		if(confirm("Napusti seksiju?")){
			this.backend.deleteSection(this.sekcijaUid).subscribe((res) => {
				if(res.code == 0){
					this.router.navigate(["/"]);
				}else{
					alert(BackendService.getMessage(res));
				}
			});
		}
	}
}