import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoviDogadajComponent } from './novi-dogadaj.component';

describe('NoviDogadajComponent', () => {
  let component: NoviDogadajComponent;
  let fixture: ComponentFixture<NoviDogadajComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoviDogadajComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoviDogadajComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
