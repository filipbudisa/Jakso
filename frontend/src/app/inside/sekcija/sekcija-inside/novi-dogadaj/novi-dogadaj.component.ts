import {Component, HostBinding, Input, OnInit} from '@angular/core';
import {SekcijaInsideComponent} from '../sekcija-inside.component';
import {Message} from '../../../../templateClasses/message';
import {Notification} from '../../../../dataTypes/model/notification.interface';
import {Returnable} from '../../../../dataTypes/returnable.interface';
import {BackendService} from '../../../../backend.service';
import {Event} from '../../../../dataTypes/model/event.interface';
import {NewEventResult} from '../../../../dataTypes/results/newEventResult.interface';

@Component({
	selector: 'app-novi-dogadaj',
	templateUrl: './novi-dogadaj.component.html',
	styleUrls: ['./novi-dogadaj.component.css']
})
export class NoviDogadajComponent extends Message implements OnInit {
	title: string = "";
	location: string = "";
	text: string = "";
	mandatory: boolean = false;
	time: string = "";

	@Input() sekcijaInside: SekcijaInsideComponent;

	constructor(private backend: BackendService) {
		super();
	}

	ngOnInit() {
	}

	objavi() {
		this.printMessage();

		if(this.title == "" || this.text == "" || this.location == "" || this.time == ""){
			this.printMessage("Sva polja su obavezna.");
			return;
		}

		this.backend.newEvent(this.sekcijaInside.sekcijaUid, this.title, this.text, this.location, this.mandatory, this.time).subscribe((res: NewEventResult) => {
			if(res.code == 0){
				this.sekcijaInside.events.unshift(<Event> {
					name: this.title,
					text: this.text,
					time: this.time,
					location: this.location,
					required: this.mandatory,
					uid: res.eventUid
				});

				this.title = "";
				this.text = "";
				this.location = "";
				this.time = "";
				this.mandatory = false;
				this.sekcijaInside.ugasiDogadaj();
			}else if(res.code != -1){
				this.printMessage(BackendService.getMessage(res));
			}
		});
	}

	odustani(){
		this.title = "";
		this.text = "";
		this.location = "";
		this.time = "";
		this.mandatory = false;
		this.sekcijaInside.ugasiDogadaj();
	}
}
