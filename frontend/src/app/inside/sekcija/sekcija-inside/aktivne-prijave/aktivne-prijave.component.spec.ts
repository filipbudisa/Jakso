import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AktivnePrijaveComponent } from './aktivne-prijave.component';

describe('AktivnePrijaveComponent', () => {
  let component: AktivnePrijaveComponent;
  let fixture: ComponentFixture<AktivnePrijaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AktivnePrijaveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AktivnePrijaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
