import {Component, Input, OnInit} from '@angular/core';
import {SekcijaInsideComponent} from '../sekcija-inside.component';
import {Application} from '../../../../dataTypes/model/application.interface';

@Component({
	selector: 'app-aktivne-prijave',
	templateUrl: './aktivne-prijave.component.html',
	styleUrls: ['./aktivne-prijave.component.css']
})
export class AktivnePrijaveComponent implements OnInit {
	@Input() sekcijaInside: SekcijaInsideComponent;
	@Input() prijave: (Application & {
		sectionName: string,
		userName: string,
		status: number
	})[] = [];

	readonly statusMsg: string[] = [
		"Otvorena",
		"Odbijena",
		"Prihvaćena",
		""
	];

	constructor() {
	}

	ngOnInit() {
	}

	ugasi(){
		this.sekcijaInside.ugasiPrijave();
	}
}
