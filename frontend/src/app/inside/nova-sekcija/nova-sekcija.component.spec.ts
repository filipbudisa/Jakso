import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NovaSekcijaComponent } from './nova-sekcija.component';

describe('NovaSekcijaComponent', () => {
  let component: NovaSekcijaComponent;
  let fixture: ComponentFixture<NovaSekcijaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovaSekcijaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovaSekcijaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
