import {Component, OnInit} from '@angular/core';
import {Message} from '../../templateClasses/message';
import {BackendService} from '../../backend.service';
import {Returnable} from '../../dataTypes/returnable.interface';
import {DataStoreService} from '../../data-store.service';
import {Router} from '@angular/router';

@Component({
	selector: 'app-nova-sekcija',
	templateUrl: './nova-sekcija.component.html',
	styleUrls: ['./nova-sekcija.component.css']
})
export class NovaSekcijaComponent extends Message implements OnInit {
	name: string = "";
	description: string = "";
	location: string = "";

	constructor(private backend: BackendService, private data: DataStoreService, private router: Router) {
		super();
	}

	ngOnInit() {
	}

	public novaSekcija(): void {
		if(this.name == "" || this.description == "" || this.location == ""){
			this.printMessage("Sva polja su obavezna!");
			return;
		}

		this.backend.newSection(this.name, this.location, this.description).subscribe((result: Returnable & { sectionUid: string }) => {
			if(result.code != 0){
				this.printMessage(BackendService.getMessage(result));
				return;
			}

			this.name = this.description = this.location = "";
			this.data.updateSections();
			this.router.navigate(["/sekcija", result.sectionUid]);
		});
	}
}
