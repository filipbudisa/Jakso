import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PregledComponent} from './pregled/pregled.component';
import { HeaderComponent } from './header/header.component';
import { SekcijaComponent } from './sekcija/sekcija.component';
import {AppRouterModule} from '../app-router.module';
import { SekcijaInsideComponent } from './sekcija/sekcija-inside/sekcija-inside.component';
import { SekcijaOutsideComponent } from './sekcija/sekcija-outside/sekcija-outside.component';
import { NovaObavijestComponent } from './sekcija/sekcija-inside/nova-obavijest/nova-obavijest.component';
import { NoviDogadajComponent } from './sekcija/sekcija-inside/novi-dogadaj/novi-dogadaj.component';
import { IzmjenaTerminaComponent } from './sekcija/sekcija-inside/izmjena-termina/izmjena-termina.component';
import { NovaSekcijaComponent } from './nova-sekcija/nova-sekcija.component';
import {FormsModule} from '@angular/forms';
import { PostavkeSekcijeComponent } from './sekcija/sekcija-inside/postavke-sekcije/postavke-sekcije.component';
import { PretragaComponent } from './pretraga/pretraga.component';
import { VanredniTerminaComponent } from './sekcija/sekcija-inside/vanredni-termina/vanredni-termina.component';
import { PrijaveComponent } from './prijave/prijave.component';
import { PrijavaComponent } from './prijava/prijava.component';
import { DogadajDolazniciComponent } from './sekcija/sekcija-inside/dogadaj-dolaznici/dogadaj-dolaznici.component';
import { AktivnePrijaveComponent } from './sekcija/sekcija-inside/aktivne-prijave/aktivne-prijave.component';
import { SekcijaPrijavaComponent } from './sekcija-prijava/sekcija-prijava.component';

@NgModule({
	imports: [
		FormsModule,
		CommonModule,
		AppRouterModule
	],
	declarations: [PregledComponent, HeaderComponent, SekcijaComponent, SekcijaInsideComponent, SekcijaOutsideComponent, NovaObavijestComponent, NoviDogadajComponent, IzmjenaTerminaComponent, NovaSekcijaComponent, PostavkeSekcijeComponent, PretragaComponent, VanredniTerminaComponent, PrijaveComponent, PrijavaComponent, DogadajDolazniciComponent, AktivnePrijaveComponent, SekcijaPrijavaComponent],
    exports: [PregledComponent, HeaderComponent, SekcijaComponent, NovaSekcijaComponent, PretragaComponent, PrijaveComponent, PrijavaComponent, SekcijaPrijavaComponent]
})
export class InsideModule {
}
