import {Component, OnInit} from '@angular/core';
import {BackendService} from '../../backend.service';
import {SessionService} from '../../session.service';
import {OverviewResult} from '../../dataTypes/results/overviewResult.interface';
import {Event} from '../../dataTypes/model/event.interface';
import {Notification} from '../../dataTypes/model/notification.interface';
import {Section} from '../../dataTypes/model/section.interface';
import {DataStoreService} from '../../data-store.service';
import {DatePipe} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import * as moment from 'moment';

@Component({
	selector: 'app-pregled',
	templateUrl: './pregled.component.html',
	styleUrls: ['./pregled.component.css']
})
export class PregledComponent implements OnInit {
	notifications: Notification[] = [];
	events: Event[] = [];
	hasSections: boolean = false;

	public moment: any = moment;

	constructor(private backend: BackendService, private session: SessionService, private data: DataStoreService, private route: ActivatedRoute) {
	}

	ngOnInit() {
		this.route.paramMap.subscribe(params => {
			this.ucitajPregled();
		});
	}

	ucitajPregled(){
		console.log("ucitavam");

		this.data.updateSections();

		this.backend.getOverview(this.session.getUid()).subscribe(result => {
			let data: OverviewResult = result;

			this.hasSections = data.user.joinedSections.length > 0;

			this.notifications = [];
			this.events = [];

			if(!this.hasSections) return;

			data.user.joinedSections.forEach(section => {
				if(section.notification.title != undefined) {
					section.notification.section = section;
					this.notifications.push(section.notification);
				}

				if(section.event.name != undefined) {
					section.event.section = section;
					this.events.push(section.event);
				}
			});
		});
	}

}
