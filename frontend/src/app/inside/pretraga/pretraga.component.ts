import {Component, OnInit} from '@angular/core';
import {Section} from '../../dataTypes/model/section.interface';
import {ActivatedRoute} from '@angular/router';
import {BackendService} from '../../backend.service';
import {SearchResult} from '../../dataTypes/results/searchResult.interface';
import {DataStoreService} from '../../data-store.service';

@Component({
	selector: 'app-pretraga',
	templateUrl: './pretraga.component.html',
	styleUrls: ['./pretraga.component.css']
})
export class PretragaComponent implements OnInit {
	sekcije1: Section[] = [];
	sekcije2: Section[] = [];

	term: string = "tekst pretrage (wip)";
	trazim: boolean = true;

	constructor(private route: ActivatedRoute, private backend: BackendService, private data: DataStoreService) {
	}

	ngOnInit() {
		this.data.updateSections();

		this.route.paramMap.subscribe(params => {
			this.term = params.get("term");
			this.pretraga();
		});
	}

	pretraga() {
		this.sekcije1 = [];
		this.sekcije2 = [];

		this.trazim = true;
		this.backend.search(this.term).subscribe((res: SearchResult) => {
			this.trazim = false;

			let i: number = 0;
			res.sections.forEach(section => {
				if(i++%2) this.sekcije1.push(section);
				else this.sekcije2.push(section);
			});
		});
	}
}
