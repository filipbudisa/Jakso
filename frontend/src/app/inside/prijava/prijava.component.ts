import {Component, OnInit} from '@angular/core';
import {Section} from '../../dataTypes/model/section.interface';
import {UserResult} from '../../dataTypes/results/userResult.interface';
import {BackendService} from '../../backend.service';
import {ApplicationResult} from '../../dataTypes/results/applicationResult.interface';
import {SekcijaResult} from '../../dataTypes/results/sekcijaResult.interface';
import {ActivatedRoute, Router} from '@angular/router';
import {SessionService} from '../../session.service';
import {Returnable} from '../../dataTypes/returnable.interface';
import {NewApplicationResult} from '../../dataTypes/results/newApplicationResult.interface';

@Component({
	selector: 'app-prijava',
	templateUrl: './prijava.component.html',
	styleUrls: ['./prijava.component.css']
})
export class PrijavaComponent implements OnInit {
	sekcija: Section = { uid: "", name: "" };
	vlasnik: { name: string, surname: string, uid: string } = { name: "", surname: "", uid: "" };
	status: number = 3;
	prijava: string;

	poruke: { time: string, text: string, sender: string, owner?: boolean, class?: string }[] = [];

	poruka: string = "";

	prijavaJeNova: boolean;

	readonly statusMsg: string[] = [
		"Otvorena",
		"Odbijena",
		"Prihvaćena",
		"",
		"Nova prijava"
	];

	constructor(private backend: BackendService, private route: ActivatedRoute, private session: SessionService, private router: Router) {
	}

	ngOnInit() {
		this.route.paramMap.subscribe(params => {
			this.prijava = params.get("uid");

			if(this.prijava == "nova"){
				this.prijavaJeNova = true;
				this.novaPrijava(params.get("uid2"));
			}else{
				this.prijavaJeNova = false;
				this.ucitajPrijavu(this.prijava);
			}
		});
	}

	novaPrijava(sekcijaUid: string){
		this.status = 4;

		this.backend.getSection(sekcijaUid).subscribe((res: SekcijaResult) => {
			this.sekcija = res.section;

			this.backend.getUser(sekcijaUid).subscribe((res: UserResult) => {
				this.vlasnik = res.user;
			});
		});
	}

	ucitajPrijavu(uid: string){
		this.sekcija = { uid: "", name: "" };
		this.vlasnik = { name: "", surname: "", uid: "" };
		this.status = 3;

		this.backend.getApplication(uid).subscribe((res: ApplicationResult) => {
			this.status = res.application.status;

			this.backend.getSection(res.application.sectionUid).subscribe((sekRes: SekcijaResult) => {
				this.sekcija = sekRes.section;
				this.vlasnik.uid = this.sekcija.creator;

				this.backend.getUser(this.vlasnik.uid).subscribe((usRes: UserResult) => {
					this.vlasnik = usRes.user;
				});

				this.poruke = res.application.messages;
				this.poruke.push({ text: res.application.text, sender: res.application.userUid, time: "" });
				this.poruke.forEach(p => {
					p.owner = p.sender == this.vlasnik.uid;
					p.class = p.owner ? "box poruka owner" : "box poruka";
				});
			});
		});
	}

	posaljiPoruku(){
		if(this.status == 4){
			this.backend.newApplication(this.sekcija.uid, this.session.getUid(), this.poruka).subscribe((res: NewApplicationResult) => {
				if(res.code == 0){
					this.router.navigate(["/prijava", res.applicationUid]);
				}
			});
		}else{
			this.backend.applicationMessage(this.prijava, this.session.getUid(), this.poruka).subscribe((res: Returnable) => {
				if(res.code == 0){
					this.poruke.unshift({ text: this.poruka, time: Date.now().toLocaleString(),
						sender: this.session.getUid(), class: "box poruka",
						owner: false });
					this.poruka = "";
				}else if(res.code != -1){
					alert(BackendService.getMessage(res));
				}
			});
		}
	}
}
