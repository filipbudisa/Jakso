import {Component, OnInit} from '@angular/core';
import {SessionService} from '../../session.service';
import {Router} from '@angular/router';
import {BackendService} from '../../backend.service';
import {UserResult} from '../../dataTypes/results/userResult.interface';
import {DataStoreService} from '../../data-store.service';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
	term: string = "";

	constructor(private sessionService: SessionService, private router: Router, public data: DataStoreService) {
	}

	ngOnInit() {
		this.data.updateSections();
	}

	doLogout(){
		this.data.clear();
		this.sessionService.clearSession();
		this.router.navigate(["/login"]);
	}

	pretraga(){
		this.router.navigate(["/pretraga", this.term]);
	}
}
