import {Component, OnInit} from '@angular/core';
import {Application} from '../../dataTypes/model/application.interface';
import {BackendService} from '../../backend.service';
import {SessionService} from '../../session.service';
import {ApplicationsResult} from '../../dataTypes/results/applicationsResult.interface';

@Component({
	selector: 'app-prijave',
	templateUrl: './prijave.component.html',
	styleUrls: ['./prijave.component.css']
})
export class PrijaveComponent implements OnInit {
	prijave: (Application & {
		sectionName: string,
		userName: string,
		status: number
	})[] = [];

	readonly status: string[] = [
		"Otvorena",
		"Odbijena",
		"Prihvaćena"
	];

	constructor(private session: SessionService, private backend: BackendService) {
	}

	ngOnInit() {
		this.prijave = [];

		this.backend.getApplications(undefined, this.session.getUid()).subscribe((res: ApplicationsResult) => {
			this.prijave = res.applications;
		});
	}


}
