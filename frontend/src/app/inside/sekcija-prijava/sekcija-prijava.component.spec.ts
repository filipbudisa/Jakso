import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SekcijaPrijavaComponent } from './sekcija-prijava.component';

describe('SekcijaPrijavaComponent', () => {
  let component: SekcijaPrijavaComponent;
  let fixture: ComponentFixture<SekcijaPrijavaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SekcijaPrijavaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SekcijaPrijavaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
