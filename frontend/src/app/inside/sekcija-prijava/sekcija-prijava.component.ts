import {Component, OnInit} from '@angular/core';
import {Section} from '../../dataTypes/model/section.interface';
import {ActivatedRoute, Router} from '@angular/router';
import {BackendService} from '../../backend.service';
import {ApplicationResult} from '../../dataTypes/results/applicationResult.interface';
import {UserResult} from '../../dataTypes/results/userResult.interface';
import {SekcijaResult} from '../../dataTypes/results/sekcijaResult.interface';
import {SessionService} from '../../session.service';
import {Returnable} from '../../dataTypes/returnable.interface';

@Component({
	selector: 'app-sekcija-prijava',
	templateUrl: './sekcija-prijava.component.html',
	styleUrls: ['./sekcija-prijava.component.css']
})
export class SekcijaPrijavaComponent implements OnInit {
	sekcija: Section = {uid: '', name: ''};
	korisnik: { name: string, surname: string, uid: string } = {name: '', surname: '', uid: ''};
	status: number = 3;
	poruke: { time: string, text: string, sender: string, owner?: boolean, class?: string }[] = [];

	prijavaUid: string;
	sekcijaUid: string;

	readonly statusMsg: string[] = [
		"Otvorena",
		"Odbijena",
		"Prihvaćena",
		""
	];

	public poruka: string;

	constructor(private route: ActivatedRoute, private backend: BackendService, private session: SessionService, private router: Router) {

	}

	ngOnInit() {
		this.route.paramMap.subscribe(params => {
			this.prijavaUid = params.get("prijavaUid");
			this.sekcijaUid = params.get("sekcijaUid");


			this.ucitajPrijavu();
		});
	}

	odgovoriPrijava(primljen: boolean = false){
		this.backend.applicationRespond(this.prijavaUid, primljen).subscribe((res: Returnable) => {
			if(res.code == 0){
				this.router.navigate(["/sekcija", this.sekcijaUid]);
			}else if(res.code != -1){
				alert(BackendService.getMessage(res));
			}
		});
	}

	posaljiPoruku(){
		this.backend.applicationMessage(this.prijavaUid, this.session.getUid(), this.poruka).subscribe((res: Returnable) => {
			if(res.code == 0){
				this.poruke.unshift({ text: this.poruka, time: Date.now().toLocaleString(),
					sender: this.session.getUid(), class: "box poruka",
					owner: false });
				this.poruka = "";
			}else if(res.code != -1){
				alert(BackendService.getMessage(res));
			}
		});
	}

	ucitajPrijavu(){


		this.backend.getApplication(this.prijavaUid).subscribe((res: ApplicationResult) => {
			this.status = res.application.status;

			this.backend.getSection(res.application.sectionUid).subscribe((sekRes: SekcijaResult) => {
				this.sekcija = sekRes.section;
				this.korisnik.uid = res.application.userUid;

				this.backend.getUser(res.application.userUid).subscribe((usRes: UserResult) => {
					this.korisnik = usRes.user;
				});

				this.poruke = res.application.messages;
				this.poruke.push({ text: res.application.text, sender: res.application.userUid, time: "" });
				this.poruke.forEach(p => {
					p.owner = p.sender == this.korisnik.uid;
					p.class = p.owner ? "box poruka owner" : "box poruka";
				});
			});
		});
	}

}
