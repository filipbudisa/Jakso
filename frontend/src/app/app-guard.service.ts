import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {SessionService} from './session.service';

@Injectable()
export class AppGuardService implements CanActivate {
	constructor(private session: SessionService, private router: Router) {}

	public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
		let loggedIn: boolean = this.session.checkSession();

		if(loggedIn) return true;

		this.router.navigate(["/login"]);
		return false;
	}
}
