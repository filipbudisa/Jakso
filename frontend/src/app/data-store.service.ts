import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {SessionService} from './session.service';
import {Section} from './dataTypes/model/section.interface';
import {BackendService} from './backend.service';
import {UserResult} from './dataTypes/results/userResult.interface';
import {Application} from './dataTypes/model/application.interface';
import {ApplicationsResult} from './dataTypes/results/applicationsResult.interface';

@Injectable()
export class DataStoreService {
	public sections: Section[] = [];
	public imaPrijave: boolean = false;
	public korisnik: string = "";

	public accCreated: boolean = false;

	constructor(private session: SessionService, private backend: BackendService) {}

	public clear(){
		this.sections = [];
		this.korisnik = "";
	}


	public updateSections(): void {
		this.sections = [];

		this.backend.getUser(this.session.getUid()).subscribe((res: UserResult) => {
			this.sections = res.user.joinedSections;

			if(this.korisnik == "")
				this.korisnik = "" + res.user.name + " " + res.user.surname;
		});

		this.backend.getApplications(undefined, this.session.getUid()).subscribe((res: ApplicationsResult) => {
			this.imaPrijave = res.applications.length != 0;
		});
	}
}
