import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {GateModule} from './gate/gate.module';
import {AppRouterModule} from './app-router.module';
import {SessionService} from './session.service';
import {InsideModule} from './inside/inside.module';
import {AppGuardService} from './app-guard.service';
import {RouterModule} from '@angular/router';
import {BackendService} from './backend.service';
import {HttpClientModule} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';
import {DataStoreService} from './data-store.service';
import * as moment from 'moment';

@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		BrowserModule,
		HttpClientModule,
		GateModule,
		InsideModule,
		AppRouterModule
	],
	exports: [
		AppRouterModule,
		RouterModule
	],
	providers: [DataStoreService, CookieService, SessionService, BackendService, AppGuardService],
	bootstrap: [AppComponent]
})
export class AppModule {
	constructor(){
		moment.locale("hr")
	}
}
