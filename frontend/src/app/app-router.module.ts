import {NgModule} from '@angular/core';
import {LoginComponent} from './gate/login/login.component';
import {RegisterComponent} from './gate/register/register.component';
import {RouterModule, Routes} from '@angular/router';
import {PregledComponent} from './inside/pregled/pregled.component';
import {AppGuardService} from './app-guard.service';
import {SekcijaComponent} from './inside/sekcija/sekcija.component';
import {NovaSekcijaComponent} from './inside/nova-sekcija/nova-sekcija.component';
import {PretragaComponent} from './inside/pretraga/pretraga.component';
import {PrijaveComponent} from './inside/prijave/prijave.component';
import {PrijavaComponent} from './inside/prijava/prijava.component';
import {SekcijaPrijavaComponent} from './inside/sekcija-prijava/sekcija-prijava.component';

const appRoutes: Routes = [
	{
		path: 'login',
		component: LoginComponent
	},
	{
		path: 'register',
		component: RegisterComponent
	},
	{
		path: 'sekcija/nova',
		component: NovaSekcijaComponent,
		canActivate: [AppGuardService]
	},
	{
		path: 'sekcija/:sekcijaUid/prijava/:prijavaUid',
		component: SekcijaPrijavaComponent,
		canActivate: [AppGuardService]
	},
	{
		path: 'sekcija/:uid',
		component: SekcijaComponent,
		canActivate: [AppGuardService]
	},
	{
		path: 'pretraga/:term',
		component: PretragaComponent,
		canActivate: [AppGuardService]
	},
	{
		path: 'prijave',
		component: PrijaveComponent,
		canActivate: [AppGuardService]
	},
	{
		path: 'prijava/:uid/:uid2',
		component: PrijavaComponent,
		canActivate: [AppGuardService]
	},
	{
		path: 'prijava/:uid',
		component: PrijavaComponent,
		canActivate: [AppGuardService]
	},
	{
		path: '',
		component: PregledComponent,
		canActivate: [AppGuardService]
	}
];

@NgModule({
	imports: [
		RouterModule.forRoot(appRoutes),
	],
	exports: [RouterModule],
	declarations: []
})
export class AppRouterModule {
}
