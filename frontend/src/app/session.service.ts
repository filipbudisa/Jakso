import {Injectable} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';

@Injectable()
export class SessionService {
	private sessionExists: boolean = null;
	private uid: string = null;

	constructor(private cookieService: CookieService){

	}

	public checkSession(): boolean {
		if(this.sessionExists == null){
			this.retrieveSession();

			// provjeriti /login
		}

		// ubuduće, ako kod otvaranja nećega dogijemo gresku sesije, staviti sessionExists na false

		return this.sessionExists;
	}

	public setSession(uid: string): void {
		this.uid = uid;
		this.sessionExists = true;

		this.storeSession();
	}

	public clearSession(): void {
		this.uid = null;
		this.sessionExists = false;

		this.cookieService.delete("uid");
	}

	private storeSession(): void {
		this.cookieService.set("uid", this.uid);
	}

	private retrieveSession(): void {
		if(this.cookieService.check("uid")){
			this.setSession(this.cookieService.get("uid"));
		}else{
			this.clearSession();
		}
	}

	public getUid(): string {
		return this.uid;
	}
}
