import {Returnable} from '../returnable.interface';
import {Section} from '../model/section.interface';

export interface SearchResult extends Returnable {
	sections: Section[]
}