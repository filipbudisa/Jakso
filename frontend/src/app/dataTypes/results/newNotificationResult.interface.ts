import {Returnable} from '../returnable.interface';

export interface NewNotificationResult extends Returnable {
	notificationUid: string;
}