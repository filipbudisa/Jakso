import {Returnable} from '../returnable.interface';
import {Notification} from '../model/notification.interface';

export interface NotificationsResult extends Returnable {
	notifications: Notification[],
	more: boolean
}