import {Returnable} from '../returnable.interface';
import {Application} from '../model/application.interface';

export interface ApplicationsResult extends Returnable {
	applications: (Application & {
		sectionName: string,
		userName: string,
		status: number
	})[]
}