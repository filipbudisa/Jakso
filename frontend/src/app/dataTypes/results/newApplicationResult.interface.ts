import {Returnable} from '../returnable.interface';

export interface NewApplicationResult extends Returnable {
	applicationUid: string;
}