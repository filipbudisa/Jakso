import {Returnable} from '../returnable.interface';

export interface NewEventResult extends Returnable {
	eventUid: string;
}