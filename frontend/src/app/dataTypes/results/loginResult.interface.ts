import {Returnable} from '../returnable.interface';

export interface LoginResult extends Returnable {
	user: {
		uid: string;
	}
}