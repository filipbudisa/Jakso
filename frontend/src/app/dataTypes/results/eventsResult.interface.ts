import {Returnable} from '../returnable.interface';
import {Event} from '../model/event.interface';

export interface EventsResult extends Returnable {
	events: Event[],
	more: boolean
}