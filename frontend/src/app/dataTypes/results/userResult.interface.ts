import {Returnable} from '../returnable.interface';
import {Section} from '../model/section.interface';

export interface UserResult extends Returnable {
	user: {
		uid: string,
		name: string,
		surname: string,
		email: string,
		birthdate: string,

		joinedSections: Section[]
	}
}