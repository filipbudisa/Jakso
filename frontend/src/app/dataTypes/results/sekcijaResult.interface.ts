import {Returnable} from '../returnable.interface';
import {Section} from '../model/section.interface';
import {Weekly} from '../model/weekly.interface';
import {WeeklyChange} from '../model/weekly-change.interface';
import {Application} from '../model/application.interface';

export interface SekcijaResult extends Returnable {
	section: Section,
	isMember: boolean,
	hasApplication: boolean;
	application: Application
}