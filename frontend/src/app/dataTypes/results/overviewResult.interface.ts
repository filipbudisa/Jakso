import {Returnable} from '../returnable.interface';
import {Notification} from '../model/notification.interface';
import {Event} from '../model/event.interface';

export interface OverviewResult extends Returnable {

	user: {
		uid: string,
		name: string,
		surname: string,
		email: string,
		birthdate: string,

		joinedSections: [
			{
				uid: string,
				name: string,
				notification: Notification,
				event: Event
			}
		]
	}
}