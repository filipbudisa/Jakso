import {Returnable} from '../returnable.interface';
import {Application} from '../model/application.interface';

export interface ApplicationResult extends Returnable {
	application: Application
}