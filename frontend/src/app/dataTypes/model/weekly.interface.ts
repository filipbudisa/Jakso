export interface Weekly {
	uid: string,
	dow: number,
	time: string
}