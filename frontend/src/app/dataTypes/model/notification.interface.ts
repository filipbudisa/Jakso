import {Section} from './section.interface';

export interface Notification {
	section?: Section,

	uid: string,
	title: string
	text: string,
	time: string
}