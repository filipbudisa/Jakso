export interface Application {
	uid: string,
	userUid: string,
	sectionUid: string,
	status: number, // 0 - aktivna, 1 - odbijena, 2 - prihvaćena

	text: string,
	messages: { sender: string, text: string, time: string }[]
}