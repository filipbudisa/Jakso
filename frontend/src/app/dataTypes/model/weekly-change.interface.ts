import {Weekly} from './weekly.interface';

export interface WeeklyChange {
	uid: string,
	dow: number,
	time: string,
	canceled: boolean,

	parent?: Weekly,
	date?: string,

	class?: string
}