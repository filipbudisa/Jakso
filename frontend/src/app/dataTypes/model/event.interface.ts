import {Section} from './section.interface';

export interface Event {
	section: Section,

	uid: string,
	name: string,
	text: string,
	time: string,
	location: string,
	required: boolean,

	coming?: boolean,
	members?: string[]
}