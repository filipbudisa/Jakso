import {Weekly} from './weekly.interface';
import {WeeklyChange} from './weekly-change.interface';

export interface Section {
	uid: string,
	name: string,
	description?: string,
	creator?: string,
	location?: string,
	acceptingMembers?: boolean,

	calendar?: {
		regularWeekly: Weekly[],
		regularWeeklyChange: WeeklyChange[]
	},

	members?: string[]
}