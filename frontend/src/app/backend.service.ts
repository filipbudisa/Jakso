import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpEvent, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Returnable} from './dataTypes/returnable.interface';
import {Router} from '@angular/router';
import {SessionService} from './session.service';
import "rxjs/add/operator/share";
import "rxjs/add/operator/publish";
import {ACObservable} from './dataTypes/ACObservable';


@Injectable()
export class BackendService {
	readonly domain: string;
	readonly options: any;

	static readonly messages: string[] = [
		"Uspjeh",		// 0
		"Nepoznato korisničko ime ili lozinka",		// Nepoznati korisnik
		"Nepoznato korisničko ime ili lozinka",		// Netočna lozinka
		"Sva polja su obavezna",
		"Nevažeći email",
		"Šifra je preslaba",
		"Email je zauzet"
	];

	static readonly endpoints: any = {
		login: "/login",
		register: "/register",
		get_user: "/get-user",
		overview: "/overview",

		new_section: "/new-section",
		get_section: "/get-section",
		get_notifications: "/get-notifications",
		get_events: "/get-next-events",

		edit_section: "/edit-section-info",
		new_notifiction: "/new-notification",
		remove_notification: "/remove-notification",
		new_event: "/new-event",
		remove_event: "/remove-event",
		event_rsvp: "/coming-to-event",

		remove_weekly: "/remove-weekly",
		new_weekly: "/new-weekly",

		new_weekly_change: "/new-weekly-change",
		remove_weekly_change: "/remove-weekly-change",

		remove_member: "/remove-member",
		delete_section: "/delete-section",

		new_application: "/new-application",
		get_applications: "/get-applications",
		get_application: "/get-application",
		application_message: "/application-message",
		application_respond: "/application-respond",

		search: "/search-sections"
	};

	public static getMessage(result: Returnable): string {
		if(result.code == -1) {
			return result.error;
		}else if(result.code == -3){
			return "Nemate ovlasti za ovu radnju."
		}else{
			return BackendService.messages[result.code];
		}
		
	}

	private url(endpoint: string): string {
		return this.domain + endpoint;
	}

	constructor(private http: HttpClient, private session: SessionService, private router: Router){
		this.options = {
			headers: new HttpHeaders({ "Content-Type": "application/json" }),
			withCredentials: true
		};

		this.domain = `http://${window.location.hostname}:8080`;
	}

	public error: HttpErrorResponse = undefined;
	public loading: boolean = false;

	private checker(endpoint: string, data?: any): ACObservable<any> {
		let result: ACObservable<any> = new ACObservable<any>(
			this.http.post(this.url(endpoint), data, this.options).publish());

		result.autoConnect(2);

		result.subscribe(data => {
			this.error = undefined;
			this.loading = false;

			if((<Returnable> data).code == -2){
				this.session.clearSession();
				this.router.navigate(["/login"]);
			}
		}, (error: HttpErrorResponse) => {
			console.log("eror");
			this.error = error;
			this.loading = false;
		});

		this.loading = true;

		return result;
	}

	public login(user: string, pass: string): Observable<any> {
		let data: any = {
			email: user,
			password: pass
		};

		return this.http.post(this.url(BackendService.endpoints.login), data, this.options);
	}

	public register(email: string, pass: string, ime: string, prezime: string, dob: string): Observable<any> {
		let data: any = {
			email: email,
			password: pass,
			name: ime,
			surname: prezime,
			birthDate: dob
		};

		return this.http.post(this.url(BackendService.endpoints.register), data, this.options);
	}

	public getUser(user: string): ACObservable<any> {
		let data: any = {
			uid: user
		};

		return this.checker(BackendService.endpoints.get_user, data);
	}

	public getOverview(user: string): ACObservable<any> {
		let data: any = {
			uid: user
		};

		return this.checker(BackendService.endpoints.overview, data);
	}

	public newSection(name: string, location: string, description: string): ACObservable<any> {
		let data: any = {
			name: name,
			location: location,
			description: description,
			creator: this.session.getUid()
		};

		return this.checker(BackendService.endpoints.new_section, data);
	}

	public getSection(uid: string): ACObservable<any> {
		let data: any = {
			uid: uid
		};

		return this.checker(BackendService.endpoints.get_section, data);
	}

	public getNotifications(uid: string, page: number): ACObservable<any> {
		let data: any = {
			uid: uid,
			page: page
		};

		return this.checker(BackendService.endpoints.get_notifications, data);
	}

	public getEvents(uid: string, page: number): ACObservable<any> {
		let data: any = {
			uid: uid,
			page: page
		};

		return this.checker(BackendService.endpoints.get_events, data);
	}

	public editSection(uid: string, name: string, description: string, location: string, accepting: boolean): ACObservable<any> {
		let data: any = {
			uid: uid,
			name: name,
			description: description,
			location: location,
			acceptingMembers: accepting
		};

		return this.checker(BackendService.endpoints.edit_section, data);
	}

	public newNotification(uid: string, title: string, text: string): ACObservable<any> {
		let data: any = {
			title: title,
			text: text,
			sectionUid: uid
		};

		return this.checker(BackendService.endpoints.new_notifiction, data);
	}

	public removeNotification(sectionUid: string, notificationUid: string): ACObservable<any> {
		let data: any = {
			notificationUid: notificationUid,
			sectionUid: sectionUid
		};

		return this.checker(BackendService.endpoints.remove_notification, data);
	}

	public newEvent(uid: string, name: string, text: string, location: string, mandatory: boolean, time: string): ACObservable<any> {
		let data: any = {
			name: name,
			text: text,
			location: location,
			required: mandatory,
			time: time,
			sectionUid: uid
		};

		return this.checker(BackendService.endpoints.new_event, data);
	}

	public removeEvent(sectionUid: string, eventUid: string): ACObservable<any> {
		let data: any = {
			eventUid: eventUid,
			sectionUid: sectionUid
		};

		return this.checker(BackendService.endpoints.remove_event, data);
	}

	public newWeekly(uid: string, dow: number, time: string): ACObservable<any> {
		let data: any = {
			dayOfTheWeek: dow,
			time: time,
			sectionUid: uid
		};

		return this.checker(BackendService.endpoints.new_weekly, data);
	}

	public removeWeekly(sectionUid: string, weeklyUid: string): ACObservable<any> {
		let data: any = {
			weeklyUid: weeklyUid,
			sectionUid: sectionUid
		};

		return this.checker(BackendService.endpoints.remove_weekly, data);
	}

	public newWeeklyChange(weeklyUid: string, sectionUid: string, canceled: boolean, dow?: number, time?: string): ACObservable<any> {
		let data: any = {
			weeklyUid: weeklyUid,
			sectionUid: sectionUid,
			canceled: canceled,
			dayOfTheWeek: dow,
			time: time
		};

		return this.checker(BackendService.endpoints.new_weekly_change, data);
	}

	public removeWeeklyChange(weeklyChangeUid: string, sectionUid: string): ACObservable<any> {
		let data: any = {
			weeklyUid: weeklyChangeUid,
			sectionUid: sectionUid
		};

		return this.checker(BackendService.endpoints.remove_weekly_change, data);
	}

	public removeMember(sectionUid: string, userUid: string): ACObservable<any> {
		let data: any = {
			userUid: userUid,
			sectionUid: sectionUid
		};

		return this.checker(BackendService.endpoints.remove_member, data);
	}

	public deleteSection(sectionUid: string): ACObservable<any> {
		let data: any = {
			uid: sectionUid
		};

		return this.checker(BackendService.endpoints.delete_section, data);
	}

	public eventRsvp(sectionUid: string, eventUid: string, userUid: string, coming: boolean): ACObservable<any> {
		let data: any = {
			userUid: userUid,
			sectionUid: sectionUid,
			eventUid: eventUid,
			coming: coming
		};

		return this.checker(BackendService.endpoints.event_rsvp, data);
	}

	public newApplication(sectionUid: string, userUid: string, text: string): ACObservable<any> {
		let data: any = {
			sectionUid: sectionUid,
			userUid: userUid,
			text: text
		};

		return this.checker(BackendService.endpoints.new_application, data);
	}

	public getApplications(sectionUid?: string, userUid?: string): ACObservable<any> {
		let data: any = {
			sectionUid: sectionUid,
			userUid: userUid
		};

		return this.checker(BackendService.endpoints.get_applications, data);
	}

	public getApplication(uid: string): ACObservable<any> {
		let data: any = {
			applicationUid: uid
		};

		return this.checker(BackendService.endpoints.get_application, data);
	}

	public applicationMessage(appUid: string, userUid: string, text: string): ACObservable<any> {
		let data: any = {
			userUid: userUid,
			applicationUid: appUid,
			text: text
		};

		return this.checker(BackendService.endpoints.application_message, data);
	}

	public applicationRespond(applicationUid: string, accept: boolean): ACObservable<any> {
		let data: any = {
			applicationUid: applicationUid,
			accept: accept
		};

		return this.checker(BackendService.endpoints.application_respond, data);
	}

	public search(term: string){
		let data: any = {
			query: term
		};

		return this.checker(BackendService.endpoints.search, data);
	}
}